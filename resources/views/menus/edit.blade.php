@extends('layouts.admin')

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
@endsection

@section('body')
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
            <h2 class="content-header-title">Edit Menu Item</h2>
        </div>
        <div class="text-xs-right">
            <a href="{{ route('menus.index') }}" class="btn btn-primary btn-min-width mr-2 mb-1">Back</a>
        </div>
    </div>
    <div class="content-body">
        <div class="card">
            <div class="card-body collapse in">
                <div class="card-block card-dashboard">
                    <form class="form-horizontal" role="form" name="modalForm" method="POST" action="{{ route('menus.update',$menu->id) }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="PATCH"/>
                        <div class="form-body">

                            <div class="form-group">
                                <label for="name">Name</label>
                                <input class="form-control{{ $errors->has('name') ? ' border-danger' : '' }}" id="name" type="text" placeholder="Name" class="form-control" name="name" value="{{ $menu->name }}" required>

                                @if ($errors->has('name'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="link">Link</label>
                                <input class="form-control{{ $errors->has('link') ? ' border-danger' : '' }}" id="link" type="text" placeholder="Link" class="form-control" name="link" value="{{ $menu->link }}" required>

                                @if ($errors->has('link'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('link') }}</strong>
                                    </div>
                                @endif
                            </div>
                            <?php $oldValue = $menu->roles->pluck('id')->toArray() ? : [] ?>
                            <div class="form-group">
                                <div class="input-group">  
                                    <label for="rolesSelect">Roles</label><br>
                                    <select class="form-control{{ $errors->has('roles') ? ' border-danger' : '' }} js-example-basic-multiple" id="rolesSelect" name="roles[]" multiple="multiple" style="width:100%">
                                        @foreach($roles as $role)
                                            <option value="{{ $role->id }}" {{ in_array($role->id,$oldValue) ? 'selected' : '' }}>{{ $role->name }}</option>
                                        @endforeach
                                    </select>
                                    <span style="color: #5f5959;font-size: 12px;">if no roles selected then menu item will be visible to all.</span>
                                </div>
                                @if ($errors->has('roles'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('roles') }}</strong>
                                    </div>
                                @endif

                                @if ($errors->has('roles.*'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('roles.*') }}</strong>
                                    </div>
                                @endif
                                
                            </div>
                            
                        </div>
                        <div class="form-actions right">
                            <button type="submit" class="btn btn-success">
                                <i class="icon-check2"></i> Save
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
   </div>
@stop

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#rolesSelect').select2({
                'placeholder': 'Select Roles'
            });
        });

    </script>
@endsection