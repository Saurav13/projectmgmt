@foreach ($menu_items as $item) 
    <li id="item_{{ $item->id }}" class="accordion">
        <div class="row tree-node">
            <div class="col-md-8">
                <strong class="menu-title"> 
                    @if($menus->where('parent_id',$item->id)->count() > 0)
                        <a href="javascript:;" class="btn btn-neutral btn-sm collapsed" data-toggle="collapse" data-target="#menu{{ $item->id }}">
                            {{-- <i class="icon-chevron-down2"></i> --}}
                        </a>&nbsp;&nbsp; 
                    @endif
                
                    {{ $item->name }}
                </strong>
            </div>

            <div class="col-md-4" style="text-align: right">
                @can('create', Menu::class)
                    <a title="add child" class="btn btn-outline-success btn-sm addMenuItem"  data-id="{{ $item->id }}">
                        <i class="icon-plus-circle"></i>
                    </a>
                @endcan
                
                @can('update', Menu::class)
                    <a title="edit" href="{{ route('menus.edit',$item->id) }}" class="btn btn-outline-warning btn-sm" >
                        <i class="icon-edit2"></i>
                    </a>
                @endcan

                @can('destroy', Menu::class)
                    <form action="{{ route('menus.destroy',$item->id) }}" method="POST" style="display:inline">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="DELETE" >

                        <button id='deleteMenu{{ $item->id }}' title="delete" type="button" class="btn btn-outline-danger btn-sm"><i class="icon-trash-o"></i></button>
                    </form>
                @endcan

            </div>
        </div>

        @if($menus->where('parent_id',$item->id)->count() > 0) 
            <ul style="list-style-type: none;"  id="menu{{ $item->id }}" class="collapse sortable">
                @include('menus.menus',[ 'menu_items' => $menus->where('parent_id',$item->id) ])

            </ul>
        @endif
    </li>
    
@endforeach
