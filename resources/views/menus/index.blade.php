@extends('layouts.admin')

@section('css')
    <style>
        .tree-node {
            border: 1px solid #dae2ea;
            margin: 2px 0;
            padding: 5px 0;
        }

        .menu-title a:after {
            font-family:Fontawesome;
            content:'\f068';
            float:left;
            font-size:10px;
            font-weight:300;
        }
        .menu-title a.collapsed:after {
            font-family:Fontawesome;
            content:'\f078';
        }
    </style>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

@endsection

@section('body')
   
    <div class="content-header row">

    </div>
    <div class="content-body">

        <div class="card">
            <div class="card-header">
                <h4 class="card-title">
                    Menu
                    @can('create', Menu::class)
                        <button class="btn btn-primary btn-md addMenuItem" style="float: right;" data-id="">Add New Menu</button>
                    @endcan
                </h4>
            </div>
            <div class="card-body collapse in">
                <div class="card-block card-dashboard">
                    <div class="row">
                        <div class="col-sm-9">
                            
                            <ul style="list-style-type: none;" id="tree-root"  class="sortable">
                                @include('menus.menus',['menu_items' => $menus->where('parent_id',0) ])
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" id="addMenu">
        <div class="modal-dialog" role="document" >

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" id="closemodal" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add New Menu</h4>
                </div>
                <div class="modal-body">
                    
                    <form class="form-horizontal" role="form" name="modalForm" method="POST" action="{{ route('menus.store') }}">
                        {{ csrf_field() }}

                        <input type="hidden" name="parent_id" id="parent_id" value="{{ old('parent_id') ? : '' }}">
                        @if ($errors->has('parent_id'))
                            <div class="alert alert-danger no-border mb-2">
                                <strong>{{ $errors->first('parent_id') }}</strong>
                            </div>
                        @endif
                        <div class="form-body">
    
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input class="form-control{{ $errors->has('name') ? ' border-danger' : '' }}" id="name" type="text" placeholder="Name" class="form-control" name="name" value="{{ old('name') }}" required>

                                @if ($errors->has('name'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="link">Link</label>
                                <input class="form-control{{ $errors->has('link') ? ' border-danger' : '' }}" id="link" type="text" placeholder="Link" class="form-control" name="link" value="{{ old('link') }}" required>

                                @if ($errors->has('link'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('link') }}</strong>
                                    </div>
                                @endif
                            </div>
                            <?php $oldValue = old('roles') ? : [] ?>
                            <div class="form-group">
                                <div class="input-group">  
                                    <label for="rolesSelect">Roles</label><br>
                                    <select class="form-control{{ $errors->has('roles') ? ' border-danger' : '' }} js-example-basic-multiple" id="rolesSelect" name="roles[]" multiple="multiple" style="width:100%">
                                        @foreach($roles as $role)
                                            <option value="{{ $role->id }}" {{ in_array($role->id,$oldValue) ? 'selected' : '' }}>{{ $role->name }}</option>
                                        @endforeach
                                    </select>
                                    <span style="color: #5f5959;font-size: 12px;">if no roles selected then menu item will be visible to all.</span>
                                </div>
                                @if ($errors->has('roles'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('roles') }}</strong>
                                    </div>
                                @endif

                                @if ($errors->has('roles.*'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('roles.*') }}</strong>
                                    </div>
                                @endif
                                
                            </div>
                            
                        </div>
                        <div class="form-actions right">
                            <button type="submit" class="btn btn-primary">
                                <i class="icon-check2"></i> Save
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
   
@endsection

@section('js')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>

    <script>
        $(document).ready(function() {

            $('.addMenuItem').on('click',function(){
                var parent_id = $(this).attr('data-id');
                $('#parent_id').val(parent_id);

                $('#addMenu').modal('show');
            });

            $('#rolesSelect').select2({
                'placeholder': 'Select Roles'
            });
        });
    </script>
    <script>
        if({{ count($errors)}} > 0){
            $('#addMenu').modal('show');
        }
    </script>

    @can('update', Menu::class)
        <script>
                $('.sortable').sortable({
                    update: function( event, ui ) {
                        var data = $(this).sortable('serialize');

                        $.ajax({
                            url: "/menus/updatePriority",
                            method: 'POST',
                            data: { _token: '{{ csrf_token() }}', data: data},
                            success: function(result){
                                
                            },
                            error: function(result){
                                $('.sortable').sortable( "cancel" );
                                swal(
                                    'Oops...',
                                    'Something went wrong!',
                                    'error'
                                )
                            }
                        });
                    }
                });
        </script>
                                                
    @endcan

@endsection