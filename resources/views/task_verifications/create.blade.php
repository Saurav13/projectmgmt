@extends('layouts.admin')

@section('css')

@endsection

@section('body')
    <div class="card">
        <div class="card-header">
            <h4 class="card-title"><a data-action="collapse">Verfication</a></h4>
            <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
            {{-- <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="collapse"><i class="{{ count($errors)>0 ? 'icon-minus4':'icon-plus4' }}"></i></a></li>
                </ul>
            </div> --}}
        </div>
        <div class="card-body collapse in">
            <div class="card-block card-dashboard">
                <form class="form" method="POST" id="AddTaskForm" action="{{ route('verifications.store',$tasklog->id) }}">
                    {{ csrf_field() }}
                    <div class="form-body">

                        <div class="form-group">
                            <label for="name">Comment</label>
                            <textarea rows="7" class="form-control{{ $errors->has('comment') ? ' border-danger' : '' }}" id="comment" type="text" placeholder="Comments" class="form-control" name="comment" required>{{ old('comment') }}</textarea>
                            @if ($errors->has('comment'))
                                <div class="alert alert-danger no-border mb-2">
                                    <strong>{{ $errors->first('comment') }}</strong>
                                </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <div class="input-group">  
                                <label for="status">Status</label><br>
                                <select class="form-control{{ $errors->has('status') ? ' border-danger' : '' }} js-example-basic-multiple" id="status" name="status" required>
                                    <option value="" selected disabled>Choose a Status</option>
                                    
                                    <option value="1">Verified</option>
                                    <option value="0">Rejected</option>
                                </select>
                            </div>
                            @if ($errors->has('status'))
                                <div class="alert alert-danger no-border mb-2">
                                    <strong>{{ $errors->first('status') }}</strong>
                                </div>
                            @endif
                        </div>

                    </div>

                    <div class="form-actions right">
                        <button type="submit" class="btn btn-primary">
                            <i class="icon-check2"></i> Save
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection