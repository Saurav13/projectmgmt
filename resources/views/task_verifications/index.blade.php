@extends('layouts.admin')

@section('css')

@endsection

@section('body')

    <div class="content-header row">

    </div>

    <div class="content-body">
        <div class="card">
            <div class="card-body">
                <div class="card-block">
                    <h4 class="card-title">Task logs  {{isset($for_task)?' for '.$for_task->title:''}}</h4>
                </div>

                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Task Name</th>
                                {{-- <th>Created By</th> --}}
                                <th>Task Comments</th>
                                <th>Files</th>
                                <th>Status</th>
                                <th>Verification Comment</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($tasklogs as $com)
                                <tr>
                                    <th scope="row">{{ $loop->iteration }}</th>
                                    <td>{{ $com->task->title }}</td>
                                    {{-- <td>{{ $com->creator->name }}</td> --}}
                                    <td>
                                        {{ $com->comments }}<br><br>
                                        
                                        <p>
                                            <strong>Project:</strong> {{$com->task->project->name }}<br>

                                            <strong>Completed</strong>: {{ $com->percent_completed }} % <br>                  

                                            <strong>Working Hours</strong>: {{ $com->working_hours }}
                                        </p>  
                                    </td>
                                    <td>
                                        @foreach($com->files as $file)
                                            <a href="{{ route('tasks.getFile',$file->filepath) }}" target="_blank">{{ $file->filepath }}</a><br>
                                        @endforeach
                                    </td>
                                    <td>
                                        <span class="tag {{ ($com->status == 7 ? 'tag-success' : 'tag-primary') }}"> {{ $com->state->status }}</span>
                                    </td>
                                    <td>
                                        {{ $com->verification ? $com->verification->comment : '-' }}
                                    </td>
                                    <td>
                                        @can('taskverify',$com)

                                            @if(!$com->verification && $com->task->status != 7)
                                                <button id="verifyLog{{ $com->id }}" data-logid="{{ $com->id }}" class="btn btn-outline-success btn-sm" title="Verify"><i class="icon-check"></i></button>

                                                <button id="rejectLog{{ $com->id }}" data-logid="{{ $com->id }}" class="btn btn-outline-danger btn-sm" title="Reject"><i class="icon-times"></i></button>
                                            @endif
                                        @endcan
                                        {{-- <a class="btn btn-outline-info btn-sm" title="Edit Verification" href="{{ route('verifications.edit',$com->id) }}"><i class="icon-check"></i></a> --}}
                
                                        {{-- <form action="{{ route('contact-us-messages.destroy',$contact->id) }}" method="POST" style="display:inline">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE" >

                                            <button id='deleteContact' title="delete" type="button" class="btn btn-outline-danger btn-sm"><i class="icon-trash-o"></i></button>
                                        </form> --}}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="text-xs-center mb-3">
                        <nav aria-label="Page navigation">
                            {{ $tasklogs->links('partials.pagination') }}
                        </nav>
                    </div>
                </div>
                
            </div>
        </div>
    </div>

    <div class="modal fade text-xs-left" id="verificationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel5" aria-hidden="true" >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form class="form" method="POST" action="{{ route('verifications.store') }}">
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <div class="form-body">
                            <h4 class="form-section">Task Verification</h4>
                            @if ($errors->has('id'))
                                <div class="alert alert-danger no-border mb-2">
                                    <strong>{{ $errors->first('id') }}</strong>
                                </div>
                            @endif
                            @if ($errors->has('status'))
                                <div class="alert alert-danger no-border mb-2">
                                    <strong>{{ $errors->first('status') }}</strong>
                                </div>
                            @endif
                            <input type="hidden" name="id" id="tasklogId" value="{{ old('id') }}" />
                            <input type="hidden" name="status" id="tasklogStatus" value="{{ old('status') }}" />
                            <div class="form-group">
                                <label for="comment">Comment</label>
                                <textarea class="form-control " rows="7" id="comment" placeholder="Comment"  name="comment" required>{{ old('comment') }}</textarea>
                                @if ($errors->has('comment'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('comment') }}</strong>
                                    </div>
                                @endif
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-outline-primary">Save</button>
                    </div>
                </form>                                    
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        if({{ count($errors)}} > 0){
            $('#verificationModal').modal('show');
        }

        $(document).ready(function(){
            $("button[id*='verifyLog']").click(function(){
                var id = $(this).data('logid');
                $('#tasklogId').val(id);
                $('#tasklogStatus').val(1);
                $('#verificationModal').modal('show');
            });

            $("button[id*='rejectLog']").click(function(){
                var id = $(this).data('logid');
                $('#tasklogId').val(id);
                $('#tasklogStatus').val(0);
                $('#verificationModal').modal('show');
            });
        });
    </script>
@endsection