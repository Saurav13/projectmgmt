@extends('layouts.admin')

@section('body')
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
            <h2 class="content-header-title">Dashboard</h2>
        </div>
    </div>
    <div class="content-body">
        <div class="row">
            @if(Auth::user()->hasRole('Super Admin'))
                <div class="col-xl-3 col-lg-6 col-xs-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-block">
                                <div class="media">
                                    <div class="media-body text-xs-left">
                                        <h3 class="pink">{{ App\Project::count() }}</h3>
                                        <span>Total Projects</span>
                                    </div>
                                    <div class="media-right media-middle">
                                        <i class="icon-line-chart pink font-large-2 float-xs-right"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6 col-xs-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-block">
                                <div class="media">
                                    <div class="media-body text-xs-left">
                                        <h3 class="teal">{{ App\Task::count() }}</h3>
                                        <span>Total Tasks</span>
                                    </div>
                                    <div class="media-right media-middle">
                                        <i class="icon-tasks teal font-large-2 float-xs-right"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="col-xl-3 col-lg-6 col-xs-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-block">
                                <div class="media">
                                    <div class="media-body text-xs-left">
                                        <h3 class="deep-orange">64.89 %</h3>
                                        <span>Conversion Rate</span>
                                    </div>
                                    <div class="media-right media-middle">
                                        <i class="icon-diagram deep-orange font-large-2 float-xs-right"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6 col-xs-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-block">
                                <div class="media">
                                    <div class="media-body text-xs-left">
                                        <h3 class="cyan">423</h3>
                                        <span>Support Tickets</span>
                                    </div>
                                    <div class="media-right media-middle">
                                        <i class="icon-ios-help-outline cyan font-large-2 float-xs-right"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> --}}
            @endif

            @can('view',App\User::class)
                <div class="col-xl-3 col-lg-6 col-xs-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-block">
                                <div class="media">
                                    <div class="media-body text-xs-left">
                                        <h3 class="deep-orange">{{ App\User::where('status','Active')->count() }}</h3>
                                        <span>Total Active Users</span>
                                    </div>
                                    <div class="media-right media-middle">
                                        <i class="icon-user1 deep-orange font-large-2 float-xs-right"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endcan

            @can('view',App\Team::class)
                <div class="col-xl-3 col-lg-6 col-xs-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-block">
                                <div class="media">
                                    <div class="media-body text-xs-left">
                                        <h3 class="cyan">{{ App\Team::count() }}</h3>
                                        <span>Total Teams</span>
                                    </div>
                                    <div class="media-right media-middle">
                                        <i class="icon-group cyan font-large-2 float-xs-right"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endcan

            @can('view',App\Project::class)
                <div class="col-xl-3 col-lg-6 col-xs-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-block">
                                <div class="media">
                                    <div class="media-body text-xs-left">
                                        <h3 class="pink">{{ App\Services\ProjectService::myProjectCount() }}</h3>
                                        <span>My Projects</span>
                                    </div>
                                    <div class="media-right media-middle">
                                        <i class="icon-line-chart pink font-large-2 float-xs-right"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endcan

            @can('view',App\Task::class)
                
                <div class="col-xl-3 col-lg-6 col-xs-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-block">
                                <div class="media">
                                    <div class="media-body text-xs-left">
                                        <h3 class="teal">{{ Auth::user()->mytasks()->count() }}</h3>
                                        <span>My Tasks</span>
                                    </div>
                                    <div class="media-right media-middle">
                                        <i class="icon-tasks teal font-large-2 float-xs-right"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endcan

            {{-- @can('view',App\User::class)
                <div class="col-xl-3 col-lg-6 col-xs-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-block">
                                <div class="media">
                                    <div class="media-body text-xs-left">
                                        <h3 class="deep-orange">{{ App\User::where('status','Active')->count() }}</h3>
                                        <span>Total Active Users</span>
                                    </div>
                                    <div class="media-right media-middle">
                                        <i class="icon-user1 deep-orange font-large-2 float-xs-right"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endcan

            @can('view',App\Team::class)
                <div class="col-xl-3 col-lg-6 col-xs-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-block">
                                <div class="media">
                                    <div class="media-body text-xs-left">
                                        <h3 class="cyan">{{ App\Team::count() }}</h3>
                                        <span>Total Teams</span>
                                    </div>
                                    <div class="media-right media-middle">
                                        <i class="icon-user1 cyan font-large-2 float-xs-right"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endcan --}}
        </div>

        <?php $myteams = Auth::user()->teams ?>
        @if($myteams->count() > 0)
            <div class="row">
                <div class="col-xl-12 col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">My Teams</h4>
                            <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-hover mb-0">
                                    <thead>
                                        <tr>
                                            <th>Team</th>
                                            <th>Members</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($myteams as $team)
                                            <tr>
                                                <td class="text-truncate">{{ $team->name }}</td>
                                                <td>
                                                    @foreach ($team->members as $member)
                                                        <p>{{ $member->name }} <span class="tag tag-info">{{ App\Role::find($member->pivot->role_id)->name }}</span></p>
                                                    @endforeach
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        
        @can('view',App\Task::class)
            <?php $mytasks = Auth::user()->mytasks()->where('tasks.status','!=',7)->where('task_assigns.status',1)->get(); ?>
            @if($mytasks->count() > 0)
                <div class="row">
                    <div class="col-xl-12 col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">My Ongoing Tasks</h4>
                                <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="card-block">
                                    <p class="m-0">My Total Ongoing Tasks: {{ $mytasks->count() }}<span class="float-xs-right"><a href="{{ route('tasks.index') }}" target="_blank">My tasks <i class="icon-arrow-right2"></i></a></span></p>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-hover mb-0">
                                        <thead>
                                            <tr>
                                                <th>Task</th>
                                                <th>Owner</th>
                                                <th>Priority</th>
                                                <th>Progress</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($mytasks as $task)
                                                <tr>
                                                    <td class="text-truncate">{{ $task->title }}</td>
                                                    <td class="text-truncate">
                                                        <span class="avatar avatar-xs"><img src="/admin-assets/app-assets/images/portrait/small/avatar-s-4.png" alt="avatar"></span> <span>{{ $task->creator->name }}</span>
                                                    </td>
                                                    <td class="text-truncate"><span class="tag tag-info">{{ $task->priority }}</span></td>
                                                    <td class="valign-middle" title="{{ $task->percent_completed }}%">
                                                        <progress value="{{ $task->percent_completed }}" max="100" class="progress progress-xs {{ $task->percent_completed < 40 ? 'progress-danger' : ($task->percent_completed < 80 ? 'progress-warning' : 'progress-success') }} m-0">{{ $task->percent_completed }}%</progress>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        @endcan

        @if(Auth::user()->hasRole('Super Admin'))        
            <?php $tasks = App\Services\TaskService::tasks() ?>
            <div class="row">
                <div class="col-xl-12 col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Recent Tasks</h4>
                            <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="card-block">
                                <p class="m-0">Total Tasks: {{ $tasks->count() }}<span class="float-xs-right"><a href="{{ route('tasks.index') }}" target="_blank">Tasks <i class="icon-arrow-right2"></i></a></span></p>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-hover mb-0">
                                    <thead>
                                        <tr>
                                            <th>Task</th>
                                            <th>Owner</th>
                                            <th>Priority</th>
                                            <th>Status</th>
                                            <th>Progress</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($tasks->take(10) as $task)
                                            <tr>
                                                <td class="text-truncate">{{ $task->title }}</td>
                                                <td class="text-truncate">
                                                    <span class="avatar avatar-xs"><img src="/admin-assets/app-assets/images/portrait/small/avatar-s-4.png" alt="avatar"></span> <span>{{ $task->creator->name }}</span>
                                                </td>
                                                <td class="text-truncate"><span class="tag tag-info">{{ $task->priority }}</span></td>
                                                <td class="text-truncate"><span class="tag {{ $task->status == 7 ? 'tag-success' : 'tag-primary' }}">{{ $task->state->status }}</span></td>
                                                <td class="valign-middle" title="{{ $task->percent_completed }}%">
                                                    <progress value="{{ $task->percent_completed }}" max="100" class="progress progress-xs {{ $task->percent_completed < 40 ? 'progress-danger' : ($task->percent_completed < 80 ? 'progress-warning' : 'progress-success') }} m-0">{{ $task->percent_completed }}%</progress>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

   </div>
@stop

@section('js')

@endsection