{{-- <a class="btn btn-outline-info btn-sm" title="View" href="{{ route('projects.show',$project->id) }}"><i class="icon-eye"></i></a> --}}
@can('update',$project)
    <a class="btn btn-outline-warning btn-sm" title="Edit" href="{{ route('projects.edit',$project->id) }}"><i class="icon-edit"></i></a>
@endcan
@can('viewcommunications',$project)
<a class="btn btn-outline-warning btn-sm" title="Edit" href="/project/listcommunications/?id={{$project->id}}"><i class="icon-android-chat"></i></a>
@endcan
@can('viewlog',$project)
<a class="btn btn-outline-warning btn-sm" title="Edit" href="/project/listlogs/?id={{$project->id}}"><i class="icon-drag"></i></a>
@endcan
{{-- @endcan  --}}
@can('destroy',$project)
    <form action="{{ route('projects.destroy',$project->id) }}" method="POST" style="display:inline">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="DELETE" >

        <button title="delete" type="button" class="btn btn-outline-danger btn-sm deleteButton"><i class="icon-trash-o"></i></button>
    </form>
@endcan
