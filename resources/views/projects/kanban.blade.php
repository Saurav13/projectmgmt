@extends('layouts.admin')

@section('css')
    <style>
        .board {
            position: relative;
            margin-left: 1%;
        }
        .board-column {
            position: absolute;
            left: 0;
            right: 0;
            width: 30%;
            margin: 0 1.5%;
            background: #f0f0f0;
            border-radius: 3px;
            z-index: 1;
        }
        .board-column.muuri-item-releasing {
            z-index: 2;
        }
        .board-column.muuri-item-dragging {
            z-index: 3;
            cursor: move;
        }
        .board-column-header {
            position: relative;
            height: 50px;
            line-height: 50px;
            overflow: hidden;
            padding: 0 20px;
            text-align: center;
            background: #333;
            color: #fff;
            border-radius: 3px 3px 0 0;
        }
        /* @media (max-width: 600px) {
            .board-column-header {
                text-indent: -1000px;
            }
        } */
        .board-column.Prospect .board-column-header {
            background: #4A9FF9;
        }
        .board-column.Cancelled .board-column-header {
            background: #DA4453;
        }
        .board-column.Confirmed .board-column-header {
            background: #2ac06d;
        }
        .board-column-content {
            position: relative;
            border: 10px solid transparent;
            /* min-height: 95px; */
        }
        .board-item {
            position: absolute;
            width: 100%;
            /* width: 170px; */
            margin: 5px 0;
        }
        .board-item.muuri-item-releasing {
            z-index: 9998;
        }
        .board-item.muuri-item-dragging {
            z-index: 9999;
            cursor: move;
        }
        .board-item.muuri-item-hidden {
            z-index: 0;
        }
        .board-item-content {
            position: relative;
            cursor: pointer;
        }
        /* @media (max-width: 600px) {
            .board-item-content {
                text-align: center;
            }
            .board-item-content span {
                display: none;
            }
        } */
    </style>
@endsection

@section('body')
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
            <h2 class="content-header-title">Projects</h2>
        </div>
        <div class="text-xs-right">
            <a href="{{ route('projects.index') }}" class="btn btn-primary btn-min-width mr-2 mb-1">Switch to List View</a>
        </div>
    </div>
    <div class="content-body">
        <section id="basic-listgroup" hidden>
            <div class="board">
                @foreach($statuses as $status)
                    <div class="card board-column {{ $status->status }}" data-status="{{ $status->id }}">
                        <div class="board-column-header">{{ $status->status }}</div>
                        <ul class="list-group board-column-content" style="min-height:50px">
                            @foreach ($projects->where('status',$status->id) as $project)
                                <li class="list-group-item board-item" data-project="{{ $project->id }}" data-status="{{ $status->id }}">
                                    <div class="board-item-content">
                                        <span>{{ $project->name }}</span>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                @endforeach
            </div>
        </section>
    </div>
    <!-- Modal -->
    <div class="modal fade text-xs-left" id="commentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel5" aria-hidden="true" >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form class="form" method="POST" action="">
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <div class="form-body">
                            <h4 class="form-section">Project Update</h4>
                            <div class="form-group">
                                <label for="comment">Comment</label>
                                <textarea class="form-control " rows="7" id="comment" placeholder="Comment"  name="description" required></textarea>
                            
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal" id="commentCancel">Cancel</button>
                        <button type="button" class="btn btn-outline-primary" id="commentSave">Save changes</button>
                    </div>
                </form>                                    
            </div>
        </div>
    </div>
 
@stop

@section('js')
        <script src="/admin-assets/hammer-2.0.8.min.js"></script>
        <script src="/admin-assets/muuri.js"></script>
        <script>
            $(document).ready(function() {
                $('#basic-listgroup').removeAttr('hidden');

                var itemContainers = [].slice.call(document.querySelectorAll('.board-column-content'));
                var columnGrids = [];
                var boardGrid;
                let originState = {};
                var grid;
                var project_id;
                var status_id;
                // Define the column grids so we can drag those
                // items around.
                itemContainers.forEach(function (container) {

                    // Instantiate column grid.
                    grid = new Muuri(container, {
                        items: '.board-item',
                        layoutDuration: 400,
                        layoutEasing: 'ease',
                        dragEnabled: true,
                        dragSort: function () {
                            return columnGrids;
                        },
                        dragSortInterval: 0,
                        dragContainer: document.body,
                        dragReleaseDuration: 400,
                        dragReleaseEasing: 'ease',
                        layoutOnResize: false
                    })
                    .on('send',function(event){
                        if(Object.keys(originState).length === 0) {
                            originState.fromGrid = event.fromGrid;
                            originState.fromIndex = event.fromIndex;
                        }
                        originState.toGrid = event.toGrid;
                        originState.toIndex = event.toIndex;
                        originState.item = event.item;


                    })
                    .on('dragStart', function (item) {
                        item.getElement().style.width = item.getWidth() + 'px';
                        item.getElement().style.height = item.getHeight() + 'px';
                    })
                    .on('dragReleaseEnd', function (item) {
                        // Let's remove the fixed width/height from the
                        // dragged item now that it is back in a grid
                        // column and can freely adjust to it's
                        // surroundings.
                        item.getElement().style.width = '';
                        item.getElement().style.height = '';
                        // Just in case, let's refresh the dimensions of all items
                        // in case dragging the item caused some other items to
                        // be different size.
                        columnGrids.forEach(function (grid) {
                            grid.refreshItems();
                        });

                        var element = $(item.getElement());
                        
                        project_id = element.data('project');
                        // /var old_status_id = element.data('status');
                        status_id = element.parent('ul').parent('div').data('status');

                        if(Object.keys(originState).length > 0 && originState.toGrid._id != originState.fromGrid._id)
                            $('#commentModal').modal({
                                backdrop: 'static',
                                keyboard: false
                            });
                    })
                    .on('layoutStart', function () {
                        // Let's keep the board grid up to date with the
                        // dimensions changes of column grids.
                        boardGrid.refreshItems().layout();
                    });

                    // Add the column grid reference to the column grids
                    // array, so we can access it later on.
                    columnGrids.push(grid);

                });

                // Instantiate the board grid so we can drag those
                // columns around.
                boardGrid = new Muuri('.board', {
                    layoutDuration: 400,
                    layoutEasing: 'ease',
                    dragEnabled: false,
                    dragSort: false,
                    dragSortInterval: 0,
                    dragStartPredicate: {
                        handle: '.board-column-header'
                    },
                    dragReleaseDuration: 400,
                    dragReleaseEasing: 'ease',
                });

                $('#commentSave').on('click',function(){
                    
                    $('#commentSave').attr('disabled','disabled');
                    $('#commentCancel').attr('disabled','disabled');
                    $.ajax({
                        url: "{{ URL::to('/projects/updateStatus') }}",
                        method: 'POST',
                        data: { _token: '{{ csrf_token() }}', status_id : status_id, project_id :project_id, description: $('#comment').val()},
                        success: function(result){
                            $("li.board-item[data-project='"+project_id+"']").attr('data-status',status_id);
                            $('#comment').val('')
                            
                            originState = {};
                            status_id = null;
                            $('#commentSave').removeAttr('disabled');
                            $('#commentCancel').removeAttr('disabled');
                            $('#commentModal').modal('hide');
                        },
                        error: function(xhr,status,text){
                            var response = xhr.responseJSON;
                            
                            if(xhr.status == 422){
                                var errors = '';

                                $.each(response.errors, function(index, value) {
                                    errors += value+'<br>';
                                });
                                
                                swal({
                                    title: 'Error!',
                                    html: errors, 
                                    type: "error"
                                });     
                            }
                            else if(xhr.status == 403)
                                swal("Forbidden!", response.message, "error");
                            else if(xhr.status == 404)
                                swal("Error 404!", 'Invalid Action', "error");
                            else if(xhr.status == 0)
                                swal("Network Error!", 'Please Check your Internet Connection.', "error");
                            else
                                swal("Error!", "Something went wrong.", "error");
                            
                            $('#commentSave').removeAttr('disabled');
                            $('#commentCancel').removeAttr('disabled');
                        }
                    });
                    
                })

                $('#commentCancel').on('click',function(){
                    var item = originState.item;

                    item.getElement().style.width = item.getWidth() + 'px';
                    item.getElement().style.height = item.getHeight() + 'px';

                    originState.toGrid.send(item, originState.fromGrid, originState.fromIndex,null);

                    columnGrids.forEach(function (grid) {
                        grid.refreshItems();
                    });
                    boardGrid.refreshItems().layout();
                    originState = {};
                    status_id = null;

                });

            });
        </script>
        
@endsection
