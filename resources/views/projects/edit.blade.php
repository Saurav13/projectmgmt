@extends('layouts.admin')

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
@endsection

@section('body')
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
            <h2 class="content-header-title">Edit Project</h2>
        </div>
        <div class="text-xs-right">
            <a href="{{ route('projects.index') }}" class="btn btn-primary btn-min-width mr-2 mb-1">Back</a>
        </div>
    </div>
    <div class="content-body">
        <div class="card">
            <div class="card-body collapse in">
                <div class="card-block card-dashboard">
                    <form class="form" method="POST" action="{{ route('projects.update',$project->id) }}">
                        {{ csrf_field() }}
                        <input name="_method" value="PATCH" hidden />
                        
                        <div class="form-body">
                            <h4 class="form-section"><i class="icon-eye6"></i> About Project</h4>

                            <div class="form-group">
                                <label for="name">Name</label>
                                <input class="form-control{{ $errors->has('name') ? ' border-danger' : '' }}" id="name" type="text" placeholder="Name" class="form-control" name="name" value="{{ $project->name }}" required autofocus>

                                @if ($errors->has('name'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea class="form-control{{ $errors->has('description') ? ' border-danger' : '' }}" placeholder="Description"  name="description" required>{{ $project->description }}</textarea>

                                @if ($errors->has('description'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                <div class="input-group">   
                                    <label for="company">Company</label>
                                    <input type="text" class="form-control{{ $errors->has('company') ? ' border-danger' : '' }}" id="company" name="company" value="{{ $project->company }}" placeholder="Company" required>
                                </div>
                                @if ($errors->has('company'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('company') }}</strong>
                                    </div>
                                @endif
                            </div>
                            <?php $oldValue = $project->teams->pluck('id')->toArray() ? : [] ?>
                            <div class="form-group">
                                <div class="input-group">  
                                    <label for="teamsSelect">Teams</label><br>
                                    <select class="form-control{{ $errors->has('password') ? ' border-danger' : '' }} js-example-basic-multiple" id="teamsSelect" name="teams[]" multiple="multiple" style="width:100%" required>
                                        @foreach($teams as $team)
                                            <option value="{{ $team->id }}" {{ in_array($team->id,$oldValue) ? 'selected' : '' }}>{{ $team->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @if ($errors->has('teams'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('teams') }}</strong>
                                    </div>
                                @endif

                                @if ($errors->has('teams.*'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('teams.*') }}</strong>
                                    </div>
                                @endif
                                
                            </div>

                         
                        </div>

                        <div class="form-actions right">
                            <button type="submit" class="btn btn-success">
                                <i class="icon-check2"></i> Save
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
   </div>
@stop

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#teamsSelect').select2({
                'placeholder': 'Select Teams'
            });
        });

    </script>
@endsection