@extends('layouts.admin')

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
@endsection

@section('body')
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
            <h2 class="content-header-title">Projects</h2>
        </div>
        
            <div class="text-xs-right">
                <a href="{{ route('projects.kanban') }}" class="btn btn-primary btn-min-width mr-2 mb-1">Switch to Kanban View</a>
            </div>

    </div>
    <div class="content-body">
        @can('create', App\Project::class)
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title"><a data-action="collapse">Add New Project</a></h4>
                    <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="{{ count($errors)>0 ? 'icon-minus4':'icon-plus4' }}"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-body collapse {{ count($errors)>0 ? 'in':'out' }}">
                    <div class="card-block card-dashboard">
                        <form class="form" method="POST" id="AddProjectForm" action="{{ route('projects.store') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-body">
                                <h4 class="form-section"><i class="icon-eye6"></i> About Project</h4>
    
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input class="form-control{{ $errors->has('name') ? ' border-danger' : '' }}" id="name" type="text" placeholder="Name" class="form-control" name="name" value="{{ old('name') }}" required>
    
                                    @if ($errors->has('name'))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </div>
                                    @endif
                                </div>
    
                                <div class="form-group">
                                    <label for="description">Description</label>
                                    <textarea rows="5" class="form-control{{ $errors->has('description') ? ' border-danger' : '' }}" placeholder="Description"  name="description" required>{{ old('description') }}</textarea>
    
                                    @if ($errors->has('description'))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ $errors->first('description') }}</strong>
                                        </div>
                                    @endif
                                </div>
    
                                <div class="form-group">
                                    <div class="input-group">   
                                        <label for="company">Company</label>
                                        <input type="text" class="form-control{{ $errors->has('company') ? ' border-danger' : '' }}" id="company" name="company" value="{{ old('company') }}" placeholder="Company" required>
                                    </div>
                                    @if ($errors->has('company'))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ $errors->first('company') }}</strong>
                                        </div>
                                    @endif
                                </div>
                                <?php $oldValue = old('teams') ? : [] ?>
                                <div class="form-group">
                                    <div class="input-group">  
                                        <label for="teamsSelect">Teams</label><br>
                                        <select class="form-control{{ $errors->has('password') ? ' border-danger' : '' }} js-example-basic-multiple" id="teamsSelect" name="teams[]" multiple="multiple" style="width:100%" required>
                                            @foreach($teams as $team)
                                                <option value="{{ $team->id }}" {{ in_array($team->id,$oldValue) ? 'selected' : '' }}>{{ $team->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @if ($errors->has('teams'))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ $errors->first('teams') }}</strong>
                                        </div>
                                    @endif

                                    @if ($errors->has('teams.*'))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ $errors->first('teams.*') }}</strong>
                                        </div>
                                    @endif
                                    
                                </div>

                                <div class="form-group">
                                    <div class="input-group">  
                                        <label for="status">Status</label><br>
                                        <select class="form-control{{ $errors->has('status') ? ' border-danger' : '' }} js-example-basic-multiple" id="status" name="status" required>
                                            <option value="" selected disabled>Choose a Status</option>
                                            @foreach($statuses as $status)
                                                <option value="{{ $status->id }}">{{ $status->status }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @if ($errors->has('status'))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ $errors->first('status') }}</strong>
                                        </div>
                                    @endif
                                </div>
                            </div>
    
                            <div class="form-actions right">
                                <button type="submit" class="btn btn-primary">
                                    <i class="icon-check2"></i> Save
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        @endcan
        
        <div class="card">
            <div class="card-body collapse in">
                <div class="card-block card-dashboard dataTable">

                    {!! $dataTable->table()  !!}
                </div>
            </div>
        </div>
   </div>
@stop

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#teamsSelect').select2({
                'placeholder': 'Select Teams'
            });
        });
    </script>

    {!! $dataTable->scripts() !!}
@endsection