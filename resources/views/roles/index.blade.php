@extends('layouts.admin')

@section('body')
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
            <h2 class="content-header-title">Roles</h2>
        </div>
    </div>
    <div class="content-body">
        @can('create', Role::class)
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title"><a data-action="collapse">Add New Role</a></h4>
                    <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="{{ count($errors)>0 ? 'icon-minus4':'icon-plus4' }}"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-body collapse {{ count($errors)>0 ? 'in':'out' }}">
                    <div class="card-block card-dashboard">
                        <form class="form" method="POST" id="AddRoleForm" action="{{ route('roles.store') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-body">
                                <h4 class="form-section"><i class="icon-eye6"></i> About Role</h4>
    
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input class="form-control{{ $errors->has('name') ? ' border-danger' : '' }}" id="name" type="text" placeholder="Name" class="form-control" name="name" value="{{ old('name') }}" required>
    
                                    @if ($errors->has('name'))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </div>
                                    @endif
                                </div>
                                <?php 
                                    $old_permissions = old('permissions') ? : [];
                                ?>
                                <div class="form-group">
                                    <label for="target_type">Permissions</label><br>
                                    @if ($errors->has('permissions'))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ $errors->first('permissions') }}</strong>
                                        </div>
                                    @endif
    
                                    @if ($errors->has('permissions.*'))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ $errors->first('permissions.*') }}</strong>
                                        </div>
                                    @endif
                                    <table class="table table-striped table-bordered table-hover" >
                                        <tbody>
                                            @foreach($permissionsTree as $name => $model)
                                                <tr>
                                                    <td><a href="javascript:void(0)" style="margin-left:10px">{{ $name }}</a></td>
                                                    <td>
                                                        <div class="input-group" style="margin-left:20px">
                                                            @foreach($model as $id => $name)
                                                                <label class="display-inline-block custom-control custom-checkbox">
                                                                    <input type="checkbox" name="permissions[]" value="{{ $id }}" {{ in_array($id,$old_permissions) ? 'checked' : '' }} class="custom-control-input">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description ml-0">{{ $name }}</span>
                                                                </label>
                                                            @endforeach
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
    
                            <div class="form-actions right">
                                <button type="submit" class="btn btn-primary">
                                    <i class="icon-check2"></i> Save
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        @endcan
        
        <div class="card">
            <div class="card-body collapse in">
                <div class="card-block card-dashboard dataTable">

                    {!! $dataTable->table()  !!}
                </div>
            </div>
        </div>
   </div>
@stop

@section('js')

    {!! $dataTable->scripts() !!}
@endsection