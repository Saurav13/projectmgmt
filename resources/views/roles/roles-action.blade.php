@if($role->name != 'Super Admin')
    @can('update',App\Role::class)
        <a class="btn btn-outline-warning btn-sm" title="Edit" href="{{ route('roles.edit',$role->id) }}"><i class="icon-edit"></i></a>
    @endcan

    @can('destroy',App\Role::class)
        <form action="{{ route('roles.destroy',$role->id) }}" method="POST" style="display:inline">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="DELETE" >

            <button title="delete" type="button" class="btn btn-outline-danger btn-sm deleteButton"><i class="icon-trash-o"></i></button>
        </form>
    @endcan
@endif