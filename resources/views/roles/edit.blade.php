@extends('layouts.admin')

@section('body')
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
            <h2 class="content-header-title">Edit Role</h2>
        </div>
        <div class="text-xs-right">
            <a href="{{ route('roles.index') }}" class="btn btn-primary btn-min-width mr-2 mb-1">Back</a>
        </div>
    </div>
    <div class="content-body">
        <div class="card">
            <div class="card-body collapse in">
                <div class="card-block card-dashboard">
                    <form class="form" method="POST" id="AddRoleForm" action="{{ route('roles.update',$role->id) }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="PATCH" />
                        <div class="form-body">
                            <h4 class="form-section"><i class="icon-eye6"></i> About Role</h4>

                            <div class="form-group">
                                <label for="name">Name</label>
                                <input class="form-control{{ $errors->has('name') ? ' border-danger' : '' }}" id="name" type="text" placeholder="Name" class="form-control" name="name" value="{{ $role->name }}" required autofocus>

                                @if ($errors->has('name'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </div>
                                @endif
                            </div>
                            <?php 
                                $old_permissions = $role->permissions->pluck('id')->toArray() ? : [];
                            ?>
                            <div class="form-group">
                                <label for="target_type">Permissions</label><br>
                                @if ($errors->has('permissions'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('permissions') }}</strong>
                                    </div>
                                @endif

                                @if ($errors->has('permissions.*'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('permissions.*') }}</strong>
                                    </div>
                                @endif
                                <table class="table table-striped table-bordered table-hover" >
                                    <tbody>
                                        @foreach($permissionsTree as $name => $model)
                                            <tr>
                                                <td><a href="javascript:void(0)" style="margin-left:10px">{{ $name }}</a></td>
                                                <td>
                                                    <div class="input-group" style="margin-left:20px">
                                                        @foreach($model as $id => $name)
                                                            <label class="display-inline-block custom-control custom-checkbox">
                                                                <input type="checkbox" name="permissions[]" value="{{ $id }}" {{ in_array($id,$old_permissions) ? 'checked' : '' }} class="custom-control-input">
                                                                <span class="custom-control-indicator"></span>
                                                                <span class="custom-control-description ml-0">{{ $name }}</span>
                                                            </label>
                                                        @endforeach
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="form-actions right">
                            <button type="submit" class="btn btn-success">
                                <i class="icon-check2"></i> Save
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
