<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title>Error 500</title>
        <link rel="apple-touch-icon" sizes="60x60" href="/admin-assets/app-assets/images/ico/apple-icon-60.png">
        <link rel="apple-touch-icon" sizes="76x76" href="/admin-assets/app-assets/images/ico/apple-icon-76.png">
        <link rel="apple-touch-icon" sizes="120x120" href="/admin-assets/app-assets/images/ico/apple-icon-120.png">
        <link rel="apple-touch-icon" sizes="152x152" href="/admin-assets/app-assets/images/ico/apple-icon-152.png">
        <link rel="shortcut icon" type="image/x-icon" href="/admin-assets/app-assets/images/ico/favicon.ico">
        <link rel="shortcut icon" type="image/png" href="/admin-assets/app-assets/images/ico/favicon-32.png">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-touch-fullscreen" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="default">
        <!-- BEGIN VENDOR CSS-->
        <link rel="stylesheet" type="text/css" href="/admin-assets/app-assets/css/bootstrap.css">
        <!-- font icons-->
        <link rel="stylesheet" type="text/css" href="/admin-assets/app-assets/fonts/icomoon.css">
        <link rel="stylesheet" type="text/css" href="/admin-assets/app-assets/fonts/flag-icon-css/css/flag-icon.min.css">
        <link rel="stylesheet" type="text/css" href="/admin-assets/app-assets/vendors/css/extensions/pace.css">
        <!-- END VENDOR CSS-->
        <!-- BEGIN ROBUST CSS-->
        <link rel="stylesheet" type="text/css" href="/admin-assets/app-assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/admin-assets/app-assets/css/app.css">
        <link rel="stylesheet" type="text/css" href="/admin-assets/app-assets/css/colors.css">
        <!-- END ROBUST CSS-->
        <!-- BEGIN Page Level CSS-->
        <link rel="stylesheet" type="text/css" href="/admin-assets/app-assets/css/core/menu/menu-types/vertical-menu.css">
        <link rel="stylesheet" type="text/css" href="/admin-assets/app-assets/css/core/menu/menu-types/vertical-overlay-menu.css">
        <link rel="stylesheet" type="text/css" href="/admin-assets/app-assets/css/pages/error.css">
        <!-- END Page Level CSS-->
        <!-- BEGIN Custom CSS-->
        <link rel="stylesheet" type="text/css" href="/admin-assets/assets/css/style.css">
        <!-- END Custom CSS-->
    </head>
    <body data-open="click" data-menu="vertical-menu" data-col="1-column" class="vertical-layout vertical-menu 1-column  blank-page blank-page">
        <!-- ////////////////////////////////////////////////////////////////////////////-->
        <div class="app-content content container-fluid">
            <div class="content-wrapper">
                <div class="content-header row">
                </div>
                <div class="content-body">
                    <section class="flexbox-container">
                        <div class="col-md-4 offset-md-4 col-xs-10 offset-xs-1">
                            <div class="card-header bg-transparent no-border pb-0">
                                <h2 class="error-code text-xs-center mb-2" style="font-size:10rem">500</h2>
                                <h3 class="text-uppercase text-xs-center">INTERNAL SERVER ERROR !</h3>
                            </div>
                            <div class="card-body collapse in">
                                <div class="text-center">
                                    <a href="/" class="btn btn-primary btn-block font-small-3"><i class="icon-home3"></i> Back to Home</a>
                                </div>
                            </div>
                        </div>
                    </section>

                </div>
            </div>
        </div>
        <!-- ////////////////////////////////////////////////////////////////////////////-->

        <!-- BEGIN VENDOR JS-->
        <script src="/admin-assets/app-assets/js/core/libraries/jquery.min.js" type="text/javascript"></script>
        <script src="/admin-assets/app-assets/vendors/js/ui/tether.min.js" type="text/javascript"></script>
        <script src="/admin-assets/app-assets/js/core/libraries/bootstrap.min.js" type="text/javascript"></script>
        <script src="/admin-assets/app-assets/vendors/js/ui/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
        <script src="/admin-assets/app-assets/vendors/js/ui/unison.min.js" type="text/javascript"></script>
        <script src="/admin-assets/app-assets/vendors/js/ui/blockUI.min.js" type="text/javascript"></script>
        <script src="/admin-assets/app-assets/vendors/js/ui/jquery.matchHeight-min.js" type="text/javascript"></script>
        <script src="/admin-assets/app-assets/vendors/js/ui/screenfull.min.js" type="text/javascript"></script>
        <script src="/admin-assets/app-assets/vendors/js/extensions/pace.min.js" type="text/javascript"></script>
        <!-- BEGIN VENDOR JS-->
        <!-- BEGIN PAGE VENDOR JS-->
        <!-- END PAGE VENDOR JS-->
        <!-- BEGIN ROBUST JS-->
        <script src="/admin-assets/app-assets/js/core/app-menu.js" type="text/javascript"></script>
        <script src="/admin-assets/app-assets/js/core/app.js" type="text/javascript"></script>
        <!-- END ROBUST JS-->
        <!-- BEGIN PAGE LEVEL JS-->
        <!-- END PAGE LEVEL JS-->
    </body>
</html>
