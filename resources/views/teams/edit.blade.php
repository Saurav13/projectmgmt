@extends('layouts.admin')

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
@endsection

@section('body')
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
            <h2 class="content-header-title">Edit Team</h2>
        </div>
        <div class="text-xs-right">
            <a href="{{ route('teams.index') }}" class="btn btn-primary btn-min-width mr-2 mb-1">Back</a>
        </div>
    </div>
    <div class="content-body">
        <div class="card">
            <div class="card-body collapse in">
                <div class="card-block card-dashboard">
                    <form class="form" method="POST" id="AddTeamForm" action="{{ route('teams.update',$team->id) }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="PATCH" />
                        <div class="form-body">
                            <h4 class="form-section"><i class="icon-eye6"></i> About Team</h4>

                            <div class="form-group">
                                <label for="name">Name</label>
                                <input class="form-control{{ $errors->has('name') ? ' border-danger' : '' }}" id="name" type="text" placeholder="Name" class="form-control" name="name" value="{{ $team->name }}" required autofocus>

                                @if ($errors->has('name'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="status">Status</label>
                                <select class="form-control{{ $errors->has('status') ? ' border-danger' : '' }}" id="status" name="status">
                                    <option value="1" {{ $team->active ? 'selected' : '' }}>Active</option>
                                    <option value="0" {{ !$team->active ? 'selected' : '' }}>Inactive</option>
                                </select>
                                
                                @if($errors->has('status'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('status') }}</strong>
                                    </div>
                                @endif
                            </div>

                            <div class="form-group" ng-app="members" ng-controller="MemberController" ng-init="init({{ json_encode($team->members->pluck('pivot')->toArray()) }})">
                                <div class="input-group">
                                    <div class="mb-2">
                                        <button class="btn btn-success btn-fill" ng-click="addMember()" type="button" rel="tooltip" title="add new" style="float:right"><i class="icon-plus"></i></button>

                                        <label for="memberSelect">Team Members and their Roles</label>
                                        
                                    </div>
                                    @if ($errors->has('members'))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ $errors->first('members') }}</strong>
                                        </div>
                                    @endif

                                    @if ($errors->has('members.*'))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ $errors->first('members.*') }}</strong>
                                        </div>
                                    @endif

                                    @if(Session::has('duplicate'))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ Session::get('duplicate') }}</strong>
                                        </div>
                                    @endif
                                    
                                    <table class="table table-striped table-bordered table-hover" >
                                        <thead>
                                            <th>Member</th>
                                            <th>Role</th>
                                            <th width="10%"></th>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="m in members">
                                                <td>
                                                    <select class="form-control js-example-basic-single" name="members[@{{ $index }}][user_id]" convert-to-number ng-model="m.user_id" style="width:100%" required>
                                                        <option value="" disabled>Select user</option>
                                                        @foreach($users as $user)
                                                            <option value="{{ $user->id }}">{{ $user->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                                <td >
                                                    <select class="form-control js-example-basic-single" name="members[@{{ $index }}][role_id]" convert-to-number ng-model="m.role_id" style="width:100%" required>
                                                        <option value="" disabled>Select role</option>

                                                        @foreach($roles as $role)
                                                            <option value="{{ $role->id }}">{{ $role->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                                <td><button ng-hide="members.length==1" type="button" class="btn btn-outline-danger btn-sm" ng-click="removeMember($index)" rel="tooltip" title="remove"><i class="icon-times"></i></button></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                
                            </div>
                            
                        </div>

                        <div class="form-actions right">
                            <button type="submit" class="btn btn-success">
                                <i class="icon-check2"></i> Save
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.7.8/angular.min.js"></script>

    <script>
        $(document).ready(function() {
            $('.js-example-basic-single').select2();
        });
    </script>

    <script>
        var l = angular.module('members',[]);
        l.controller('MemberController',function($scope){

            $scope.init = function(oldValue){
                console.log(oldValue)
  
                $scope.members = oldValue;
            }

            $scope.addMember= function()
            {
                var k= {user_id:'',role_id:''};
                $scope.members.push(k);

                $scope.$$postDigest(function () {
                    $('.js-example-basic-single').select2();
                })

            };
            
            $scope.removeMember= function(index)
            {
                $scope.members.splice(index,1);
            };
        });

        l.directive('convertToNumber', function() {
            return {
                require: 'ngModel',
                link: function(scope, element, attrs, ngModel) {
                ngModel.$parsers.push(function(val) {
                    return parseInt(val, 10);
                });
                ngModel.$formatters.push(function(val) {
                    return '' + val;
                });
                }
            };
        });
    </script>

@endsection
