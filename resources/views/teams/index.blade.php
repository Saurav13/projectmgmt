@extends('layouts.admin')

@section('body')
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
            <h2 class="content-header-title">Teams</h2>
        </div>
        @can('create', App\Team::class)
            <div class="text-xs-right">
                <a href="{{ route('teams.create') }}" class="btn btn-primary btn-min-width mr-2 mb-1">Create New Team</a>
            </div>
        @endcan
    </div>
    <div class="content-body">
        <div class="card">
            <div class="card-body collapse in">
                <div class="card-block card-dashboard dataTable">

                    {!! $dataTable->table()  !!}
                </div>
            </div>
        </div>
   </div>
@stop

@section('js')

    {!! $dataTable->scripts() !!}
@endsection