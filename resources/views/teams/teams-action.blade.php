{{-- <a class="btn btn-outline-info btn-sm" title="View" href="{{ route('teams.show',$team->id) }}"><i class="icon-eye"></i></a> --}}
@can('update',App\Team::class)
    <a class="btn btn-outline-warning btn-sm" title="Edit" href="{{ route('teams.edit',$team->id) }}"><i class="icon-edit"></i></a>
@endcan

@can('destroy',App\Team::class)
    <form action="{{ route('teams.destroy',$team->id) }}" method="POST" style="display:inline">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="DELETE" >

        <button title="delete" type="button" class="btn btn-outline-danger btn-sm deleteButton"><i class="icon-trash-o"></i></button>
    </form>
@endcan