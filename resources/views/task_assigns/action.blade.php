{{-- <a class="btn btn-outline-info btn-sm" title="View" href="{{ route('tasks.show',$task->id) }}"><i class="icon-eye"></i></a> --}}
{{-- @can('update',App\TaskAssign::class)
    <a class="btn btn-outline-warning btn-sm" title="Edit" href="{{ route('assignments.edit',$task_assign->id) }}"><i class="icon-edit"></i></a>
@endcan --}}

@can('destroy',App\TaskAssign::class)
    <form action="{{ route('assignments.destroy',$task_assign->id) }}" method="POST" style="display:inline">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="DELETE" >

        <button title="delete" type="button" class="btn btn-outline-danger btn-sm deleteButton"><i class="icon-trash-o"></i></button>
    </form>
@endcan