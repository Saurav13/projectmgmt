@extends('layouts.admin')

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
@endsection

@section('body')
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
            <h2 class="content-header-title">Task Assignments</h2>
        </div>
    </div>
    <div class="content-body">
        @can('create', App\TaskAssign::class)
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title"><a data-action="collapse">Assign New Task</a></h4>
                    <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="{{ (count($errors)>0 || Session::has('invalid_member')) ? 'icon-minus4':'icon-plus4' }}"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-body collapse {{ (count($errors)>0 || Session::has('invalid_member')) ? 'in':'out' }}">
                    <div class="card-block card-dashboard">
                        <form class="form" method="POST" id="AddTaskAssignForm" action="{{ route('assignments.store') }}">
                            {{ csrf_field() }}
                            <div class="form-body">
                                
                                <div class="form-group">
                                    <div class="input-group">  
                                        <label for="status">Project</label><br>
                                        <select class="form-control{{ $errors->has('project') ? ' border-danger' : '' }} js-example-basic-multiple" id="chooseProject" name="project" style="width:100%" required>
                                            <option value="" selected disabled>Choose a Project</option>
                                            @foreach($projects as $project)
                                                <option value="{{ $project->id }}" {{ old('project') == $project->id ? 'selected' : '' }}>{{ $project->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @if ($errors->has('project'))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ $errors->first('project') }}</strong>
                                        </div>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="task_id">Task</label><br>
                                    <select class="form-control{{ $errors->has('task_id') ? ' border-danger' : '' }} js-example-basic-multiple" id="chooseTask" name="task_id" style="width:100%"  required>
                                        <option value="" selected disabled>Select Task</option>
                                        
                                    </select>

                                    @if ($errors->has('task_id'))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ $errors->first('task_id') }}</strong>
                                        </div>
                                    @endif
                                </div>
    
                                <div class="form-group">
                                    <label for="assigned_to">Assign To</label><br>
                                    <select class="form-control{{ $errors->has('assigned_to') ? ' border-danger' : '' }} js-example-basic-multiple" id="chooseUser" name="assigned_to" style="width:100%"  required>
                                        <option value="" selected disabled>Select a Team Member</option>                                        
                                    </select>

                                    @if ($errors->has('assigned_to') || Session::has('invalid_member'))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ $errors->first('assigned_to') }}</strong>
                                            <strong>{{ Session::get('invalid_member') }}</strong>
                                            
                                        </div>
                                    @endif
                                </div>

                                {{-- <div class="form-group">
                                    <div class="input-group">  
                                        <label for="status">Status</label><br>
                                        <select class="form-control{{ $errors->has('status') ? ' border-danger' : '' }} js-example-basic-multiple" id="status" name="status" required>
                                            <option value="" selected disabled>Choose a Status</option>
                                            @foreach($statuses as $status)
                                                <option value="{{ $status->id }}" {{ old('status') == $status->id ? 'selected' : '' }}>{{ $status->status }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @if ($errors->has('status'))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ $errors->first('status') }}</strong>
                                        </div>
                                    @endif
                                </div> --}}
                            </div>
    
                            <div class="form-actions right">
                                <button type="submit" class="btn btn-primary">
                                    <i class="icon-check2"></i> Save
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        @endcan
        
        <div class="card">
            <div class="card-body collapse in">
                <div class="card-block card-dashboard dataTable">

                    {!! $dataTable->table()  !!}
                </div>
            </div>
        </div>
   </div>
@stop

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#chooseProject').select2({
                'placeholder': 'Select Project'
            });

            var project = $('#chooseProject').val();
            getTasks(project,"{{ old('task_id') }}");

            $('#chooseProject').on('change',function(){
                var project = $('#chooseProject').val();

                getTasks(project,null);
            });

            function getTasks(project,select) {
                
                if(!project) return;

                $.ajax({
                    url: "/projects/"+project+"/getTasks",
                    method: 'GET',
                    success: function(result){
                                                             
                        var html = '<option value="" selected disabled>Select a Task</option>';
                        for(var i=0;i<result.length;i++){
                            html += '<option value="'+result[i].id+'" '+ (result[i].id == select ? 'selected' : '')+'>'+result[i].title+'</option>';
                        }
                        $('#chooseTask').html(html);

                        $('#chooseTask').select2({
                            'placeholder': 'Select a Task'
                        });

                        var task = $('#chooseTask').val();
                        getTeamMembers(task,"{{ old('assigned_to') }}");
                    },
                    error: function(result){
                        swal(
                            'Oops...',
                            'Something went wrong!',
                            'error'
                        )
                    }
                });
            }

            $('#chooseTask').on('change',function(){
                var task = $('#chooseTask').val();

                getTeamMembers(task,null);
            });

            function getTeamMembers(task,select) {
                
                if(!task) return;

                $.ajax({
                    url: "/tasks/"+task+"/getMembers",
                    method: 'GET',
                    success: function(result){
                                                             
                        var html = '<option value="" selected disabled>Select a Team Member</option>';
                        for(var i=0;i<result.length;i++){
                            html += '<option value="'+result[i].id+'" '+ (result[i].id == select ? 'selected' : '')+'>'+result[i].name+'</option>';
                        }
                        $('#chooseUser').html(html);

                        $('#chooseUser').select2({
                            'placeholder': 'Select a Team Member'
                        });
                    },
                    error: function(result){
                        swal(
                            'Oops...',
                            'Something went wrong!',
                            'error'
                        )
                    }
                });
            }
        });
    </script>

    {!! $dataTable->scripts() !!}
@endsection