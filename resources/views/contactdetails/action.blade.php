{{-- <a class="btn btn-outline-info btn-sm" title="View" href="{{ route('contactdetails.show',$contactdetail->id) }}"><i class="icon-eye"></i></a> --}}
@can('update',$contactdetail)

    <a class="btn btn-outline-warning btn-sm" title="Edit" href="{{ route('contactdetails.edit',$contactdetail->id) }}"><i class="icon-edit"></i></a>
@endcan

@can('destroy',$contactdetail)
    <form action="{{ route('contactdetails.destroy',$contactdetail->id) }}" method="POST" style="display:inline">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="DELETE" >

        <button title="delete" type="button" class="btn btn-outline-danger btn-sm deleteButton"><i class="icon-trash-o"></i></button>
    </form>
@endcan

