@extends('layouts.admin')


@section('body')

<div class="card">
        <div class="card-body">
            <div class="card-block">
                <h4 class="card-title info">Contact Details</h4>
            <p class="card-text"><strong>Name:</strong> {{$contactdetail->name}}</p>
            <p class="card-text"><strong>Designation:</strong> {{$contactdetail->designation}}</p>
            <p class="card-text"><strong>Contact:</strong> {{$contactdetail->contact}}</p>
            <p class="card-text"><strong>Project:</strong> {{$contactdetail->project->name}}</p>
            <p class="card-text"><strong>Created By:</strong> {{$contactdetail->creator->name}}</p>
            <p class="card-text"><strong>Updated By:</strong> {{$contactdetail->updator->name}}</p>




            </div>
        </div>
    </div>
@stop