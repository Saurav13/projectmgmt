@extends('layouts.admin')

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
@endsection

@section('body')
    <div class="content-header row">
        
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
            <h2 class="content-header-title">Contactdetails</h2>
        </div>
    </div>
    <div class="content-body">
        @can('create', App\ContactDetail::class)
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title"><a data-action="collapse">Add New Contact Detail</a></h4>
                    <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="{{ count($errors)>0 ? 'icon-minus4':'icon-plus4' }}"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-body collapse {{ count($errors)>0 ? 'in':'out' }}">
                    <div class="card-block card-dashboard">
                        <form class="form" method="POST" id="AddContactdetailForm" action="{{ route('contactdetails.store') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-body">
                                <h4 class="form-section"><i class="icon-eye6"></i> About Contact Detail</h4>
    
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input class="form-control{{ $errors->has('name') ? ' border-danger' : '' }}" id="name" type="text" placeholder="Name" class="form-control" name="name" value="{{ old('name') }}" required>
    
                                    @if ($errors->has('name'))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </div>
                                    @endif
                                </div>
    
                                <div class="form-group">
                                    <label for="description">Designation</label>
                                    <input class="form-control{{ $errors->has('designation') ? ' border-danger' : '' }}" id="designation" type="text" placeholder="designation" class="form-control" name="designation" value="{{ old('designation') }}" required>
                                    @if ($errors->has('designation'))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ $errors->first('designation') }}</strong>
                                        </div>
                                    @endif
                                </div>
    
                                <div class="form-group">
                                    <div class="input-group">   
                                        <label for="contact">Contact</label>
                                        <input type="text" class="form-control{{ $errors->has('contact') ? ' border-danger' : '' }}" id="contact" name="contact" value="{{ old('contact') }}" placeholder="Contact" required>
                                    </div>
                                    @if ($errors->has('contact'))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ $errors->first('contact') }}</strong>
                                        </div>
                                    @endif
                                </div>
                                <?php $oldValue = old('projects') ? : [] ?>
                                <div class="form-group">
                                    <div class="input-group">  
                                        <label for="projectsSelect">Project</label><br>
                                        <select class="form-control{{ $errors->has('password') ? ' border-danger' : '' }}" id="" name="project" style="width:100%" required>
                                            @foreach($projects as $project)
                                                <option value="{{ $project->id }}" {{ in_array($project->id,$oldValue) ? 'selected' : '' }}>{{ $project->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @if ($errors->has('projects'))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ $errors->first('projects') }}</strong>
                                        </div>
                                    @endif

                                    @if ($errors->has('projects.*'))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ $errors->first('projects.*') }}</strong>
                                        </div>
                                    @endif
                                    
                                </div>

                                <div class="form-group">
                                    <div class="input-group">  
                                        <label for="status">Status</label><br>
                                        <select class="form-control{{ $errors->has('status') ? ' border-danger' : '' }} js-example-basic-multiple" id="status" name="status" required>
                                            <option value="" selected disabled>Choose a Status</option>
                                                <option value="1">Active</option>
                                                <option value="0">Inactive</option>

                                           
                                        </select>
                                    </div>
                                    @if ($errors->has('status'))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ $errors->first('status') }}</strong>
                                        </div>
                                    @endif
                                </div>
                            </div>
    
                            <div class="form-actions right">
                                <button type="submit" class="btn btn-primary">
                                    <i class="icon-check2"></i> Save
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        @endcan
        
        <div class="card">
            <div class="card-body collapse in">
                <div class="card-block card-dashboard dataTable">

                    {!! $dataTable->table()  !!}
                </div>
            </div>
        </div>
   </div>
@stop

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#projectsSelect').select2({
                'placeholder': 'Select Projects'
            });
        });
    </script>

    {!! $dataTable->scripts() !!}
@endsection