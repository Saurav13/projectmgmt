@extends('layouts.admin')

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
@endsection

@section('body')
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
            <h2 class="content-header-title">Edit Contact Detail</h2>
        </div>
        <div class="text-xs-right">
            <a href="{{ route('contactdetails.index') }}" class="btn btn-primary btn-min-width mr-2 mb-1">Back</a>
        </div>
    </div>
    <div class="content-body">
        <div class="card">
            <div class="card-body collapse in">
                <div class="card-block card-dashboard">
                    <form class="form" method="POST" action="{{ route('contactdetails.update',$contactdetail->id) }}">
                        {{ csrf_field() }}
                        <input name="_method" value="PATCH" hidden />
                        
                        <div class="form-body">
                            <h4 class="form-section"><i class="icon-eye6"></i> About Contact Detail</h4>

                            <div class="form-group">
                                <label for="name">Name</label>
                                <input class="form-control{{ $errors->has('name') ? ' border-danger' : '' }}" id="name" type="text" placeholder="Name" class="form-control" name="name" value="{{ $contactdetail->name }}" required autofocus>

                                @if ($errors->has('name'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="description">Designation</label>
                                <textarea class="form-control{{ $errors->has('designation') ? ' border-danger' : '' }}" placeholder="Designation"  name="designation" required>{{ $contactdetail->designation }}</textarea>

                                @if ($errors->has('designation'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('designation') }}</strong>
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                <div class="input-group">   
                                    <label for="contact">Contact</label>
                                    <input type="text" class="form-control{{ $errors->has('v') ? ' border-danger' : '' }}" id="contact" name="contact" value="{{ $contactdetail->contact }}" placeholder="Contact" required>
                                </div>
                                @if ($errors->has('contact'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('contact') }}</strong>
                                    </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <div class="input-group">  
                                    <label for="teamsSelect">Projects</label><br>
                                    <select class="form-control{{ $errors->has('password') ? ' border-danger' : '' }} "  name="project"  style="width:100%" required>
                                        @foreach($projects as $project)
                                            <option value="{{ $project->id }}" {{ $project->id==$contactdetail->project_id? 'selected' : '' }}>{{ $project->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @if ($errors->has('teams'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('teams') }}</strong>
                                    </div>
                                @endif

                                @if ($errors->has('teams.*'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('teams.*') }}</strong>
                                    </div>
                                @endif
                                
                            </div>

                            <div class="form-group">
                                <div class="input-group">  
                                    <label for="status">Status</label><br>
                                    <select class="form-control{{ $errors->has('status') ? ' border-danger' : '' }} js-example-basic-multiple" id="status" name="status">
                                        <option value="" selected disabled>Choose a Status</option>
                                        <option value="1" {{$contactdetail->status==1?'selected':''}}>Active</option>
                                        <option value="0" {{$contactdetail->status==0?'selected':''}}>Inactive</option>
                                    </select>
                                </div>
                                @if ($errors->has('status'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('status') }}</strong>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="form-actions right">
                            <button type="submit" class="btn btn-success">
                                <i class="icon-check2"></i> Save
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
   </div>
@stop

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#teamsSelect').select2({
                'placeholder': 'Select Teams'
            });
        });

    </script>
@endsection