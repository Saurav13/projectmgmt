@extends('layouts.admin')



@section('body')
        @can('create', App\ProjectLog::class)
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title"><a data-action="collapse">Add New Project Log</a></h4>
                    <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="{{ count($errors)>0 ? 'icon-minus4':'icon-plus4' }}"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-body collapse {{ count($errors)>0 ? 'in':'out' }}">
                    <div class="card-block card-dashboard">
                        <form class="form" method="POST" id="AddProjectLogForm" action="{{ route('projectlogs.store') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-body">
    
                                <?php $oldValue = old('projects') ? : [] ?>
                                <div class="form-group">
                                    <div class="input-group">  
                                        <label for="projectsSelect">Project</label><br>
                                        <select class="form-control{{ $errors->has('projects') ? ' border-danger' : '' }}" id="" name="project" style="width:100%" required>
                                            @foreach($projects as $project)
                                                <option value="{{ $project->id }}" {{!in_array($project->id,$oldValue) && isset($for_project) && $project->id==$for_project->id ? 'selected': ''}} {{ in_array($project->id,$oldValue) ? 'selected' : '' }}>{{ $project->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @if ($errors->has('projects'))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ $errors->first('projects') }}</strong>
                                        </div>
                                    @endif

                                    @if ($errors->has('projects.*'))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ $errors->first('projects.*') }}</strong>
                                        </div>
                                    @endif
                                    
                                </div>

                                <div class="form-group">
                                    <label for="name">Log detail</label>
                                    <textarea class="form-control{{ $errors->has('description') ? ' border-danger' : '' }}" id="description" type="text" placeholder="Description" class="form-control" name="description" value="{{ old('description') }}" required></textarea>
                                    @if ($errors->has('description'))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ $errors->first('description') }}</strong>
                                        </div>
                                    @endif
                                </div>

                             
                                <div class="form-group">
                                    <div class="input-group">   
                                        <label for="files">Files (Optional)</label>
                                        <input type="file" class="form-control{{ $errors->has('files') ? ' border-danger' : '' }}" id="files" name="files[]" placeholder="Upload Files" multiple>
                                    </div>
                                    @if ($errors->has('files'))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ $errors->first('files') }}</strong>
                                        </div>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <div class="input-group">  
                                        <label for="status">Status</label><br>
                                        <select class="form-control{{ $errors->has('status') ? ' border-danger' : '' }} js-example-basic-multiple" id="status" name="status" required>
                                            <option value="" selected disabled>Choose a Status</option>
                                            @foreach($statuses as $status)
                                                <option value="{{ $status->id }}">{{ $status->status }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @if ($errors->has('status'))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ $errors->first('status') }}</strong>
                                        </div>
                                    @endif
                                </div>


                            </div>
    
                            <div class="form-actions right">
                                <button type="submit" class="btn btn-primary">
                                    <i class="icon-check2"></i> Save
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        @endcan

    <div class="card" style="height: 497px;">
        <div class="card-body">
            <div class="card-block">
                <h4 class="card-title">Project logs  {{isset($for_project)?' for '.$for_project->name:''}}</h4>
             </div>
            <ul class="list-group list-group-flush">
                @foreach($projectlogs as $com)
                    <li class="list-group-item">
                        <p class="pull-right">
                            Project: {{$com->project->name}}<br>
                            Created By: {{$com->creator->name}}<br>
                            Created On: {{ date('M j, Y', strtotime($com->created_at)) }}<br>
                        </p>

                        <p>{{$com->description}}</p>
                        @if($com->files->count() > 0)
                            <br>
                            <span>Files: </span><br>

                            @foreach($com->files as $file)
                                <a href="{{ route('projects.getFile',$file->filepath) }}" target="_blank">{{ $file->filepath }}</a><br>
                            @endforeach
                        @endif
                        <br>
                        <p>Status: <span class="tag tag-primary">{{$com->state->status}}</span></p>
                    
                    
                    </li>
                @endforeach
            </ul>
            <div class="text-xs-center mb-3">
                    <nav aria-label="Page navigation">
                        {{ $projectlogs->links('partials.pagination') }}
                    </nav>
                </div>
        </div>
    </div>
@endsection