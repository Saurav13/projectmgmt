@extends('layouts.admin')

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
@endsection

@section('body')
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
            <h2 class="content-header-title">Edit User</h2>
        </div>
        <div class="text-xs-right">
            <a href="{{ route('users.index') }}" class="btn btn-primary btn-min-width mr-2 mb-1">Back</a>
        </div>
    </div>
    <div class="content-body">
        <div class="card">
            <div class="card-body collapse in">
                <div class="card-block card-dashboard">
                    <form class="form" method="POST" action="{{ route('users.update',$user->id) }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input name="_method" value="PATCH" hidden />
                        <div class="form-body">
                            <h4 class="form-section"><i class="icon-eye6"></i> About User</h4>

                            <div class="form-group">
                                <label for="name">Name</label>
                                <input class="form-control{{ $errors->has('name') ? ' border-danger' : '' }}" id="name" type="text" placeholder="Name" class="form-control" name="name" value="{{ $user->name }}" required autofocus>

                                @if ($errors->has('name'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="userinput5">Email</label>
                                <input class="form-control{{ $errors->has('email') ? ' border-danger' : '' }}" type="email" placeholder="Email"  name="email" value="{{ $user->email }}" required>

                                @if ($errors->has('email'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                <div class="input-group">   
                                    <label for="password">Password (Optional)</label>
                                    <input type="text" class="form-control{{ $errors->has('password') ? ' border-danger' : '' }}" id="password" name="password" placeholder="Password" rel="gp" data-size="10" data-character-set="a-z,A-Z,0-9">
                                    <span class="input-group-btn"><button type="button" class="btn btn-primary getNewPass" style="margin-top:27px" title="Generate Password"><span class="icon-reload"></span></button></span>
                                </div>
                                @if ($errors->has('password'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </div>
                                @endif
                            </div>
                            <?php $oldValue = $user->roles->pluck('id')->toArray() ? : [] ?>
                            <div class="form-group">
                                <div class="input-group">  
                                    <label for="rolesSelect">Roles</label><br>
                                    <select class="form-control{{ $errors->has('roles') ? ' border-danger' : '' }} js-example-basic-multiple" id="rolesSelect" name="roles[]" multiple="multiple" style="width:100%" required>
                                        @foreach($roles as $role)
                                            <option value="{{ $role->id }}" {{ in_array($role->id,$oldValue) ? 'selected' : '' }}>{{ $role->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @if ($errors->has('roles'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('roles') }}</strong>
                                    </div>
                                @endif

                                @if ($errors->has('roles.*'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('roles.*') }}</strong>
                                    </div>
                                @endif
                                
                            </div>

                            <div class="form-group">
                                <div class="input-group">  
                                    <label for="status">Status</label><br>
                                    <select class="form-control{{ $errors->has('status') ? ' border-danger' : '' }} js-example-basic-multiple" id="status" name="status" required>
                                        <option value="Active" {{ $user->status == 'Active' ? 'selected' : '' }}>Active</option>
                                        <option value="Inactive" {{ $user->status == 'Inactive' ? 'selected' : '' }}>Inactive</option>
                                    </select>
                                </div>
                                @if ($errors->has('status'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('status') }}</strong>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="form-actions right">
                            <button type="submit" class="btn btn-success">
                                <i class="icon-check2"></i> Save
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
   </div>
@stop

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#rolesSelect').select2({
                'placeholder': 'Select Roles'
            });
        });
        // Generate a password string
            function randString(id){
                var dataSet = $(id).attr('data-character-set').split(',');  
                var possible = '';
                if($.inArray('a-z', dataSet) >= 0){
                    possible += 'abcdefghijklmnopqrstuvwxyz';
                }
                if($.inArray('A-Z', dataSet) >= 0){
                    possible += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                }
                if($.inArray('0-9', dataSet) >= 0){
                    possible += '0123456789';
                }
                if($.inArray('#', dataSet) >= 0){
                    possible += '![]{}()%&*$#^<>~@|';
                }
                var text = '';
                for(var i=0; i < $(id).attr('data-size'); i++) {
                    text += possible.charAt(Math.floor(Math.random() * possible.length));
                }
                return text;
            }

            // Create a new password
            $(".getNewPass").click(function(){
                var field = $(this).closest('div').find('input[rel="gp"]');
                field.val(randString(field));
            });

    </script>
@endsection