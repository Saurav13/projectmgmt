{{-- <a class="btn btn-outline-info btn-sm" title="View" href="{{ route('users.show',$user->id) }}"><i class="icon-eye"></i></a> --}}
@can('update',App\User::class)
    <a class="btn btn-outline-warning btn-sm" title="Edit" href="{{ route('users.edit',$user->id) }}"><i class="icon-edit"></i></a>
@endcan

@can('destroy',App\User::class)
    <form action="{{ route('users.destroy',$user->id) }}" method="POST" style="display:inline">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="DELETE" >

        <button title="delete" type="button" class="btn btn-outline-danger btn-sm deleteButton"><i class="icon-trash-o"></i></button>
    </form>
@endcan