<p style="font-size: 18px; font-weight: 800; line-height: 24px; color: #333333;">
    Dear {{ $user->name }},
</p>
<p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #333333;">
    You have recently been added as one of the admins for '...'. To use the priviledge, please click on the link below and then sign in using this email and password.
</p>
<div style="text-align:center;margin:20px">
    <a href="{{ URL::to('login') }}" style="background-color:#23272b;color:#ffffff;display:inline-block;font-family:brandon-grotesque;text-transform: uppercase;font-size:14px;font-weight:regular;line-height:45px;text-align:center;text-decoration:none;width:155px;-webkit-text-size-adjust:none;mso-hide:all;">Login</a>
</div>

<br><br><br>
<strong>Your credentials:</strong><br><br>
<strong>Email        :</strong>{{ $user->email }} <br>
<strong>Password     :</strong>{{ $password }}<br><br><br>
<strong>Roles :</strong> @foreach($user->roles as $role) {{ $role->name }} @endforeach <br><br>

<p style="font-size: 16px; font-weight: 600; line-height: 24px; color: #333333;">                              
    Thanks<br>
    ...
</p>