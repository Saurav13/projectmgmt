@extends('layouts.admin')



@section('body')

<div class="card">
        <div class="card-header">
            <h4 class="card-title"><a data-action="collapse">Edit Communication Log</a></h4>
            <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="collapse"><i class="{{ count($errors)>0 ? 'icon-minus4':'icon-plus4' }}"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="card-body {{ count($errors)>0 ? 'in':'out' }}">
            <div class="card-block card-dashboard">
                <form class="form" method="POST" id="AddContactdetailForm" action="{{ route('communications.update',$communication->id) }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="text" name="_method" value="PUT" hidden/>
                    <div class="form-body">

                        <div class="form-group">
                            <label for="name">Communication Text</label>
                        <textarea class="form-control{{ $errors->has('description') ? ' border-danger' : '' }}" id="description" type="text" placeholder="Description" class="form-control" name="description" value="{{ old('description') }}" required>{{$communication->description}}</textarea>
                            @if ($errors->has('description'))
                                <div class="alert alert-danger no-border mb-2">
                                    <strong>{{ $errors->first('description') }}</strong>
                                </div>
                            @endif
                        </div>

                    </div>

                    <div class="form-actions right">
                        <button type="submit" class="btn btn-primary">
                            <i class="icon-check2"></i> Save
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection