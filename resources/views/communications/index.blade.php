@extends('layouts.admin')



@section('body')
    @can('create', App\ProjectCommunication::class)
        <div class="card">
            <div class="card-header">
                <h4 class="card-title"><a data-action="collapse">Add New Communication Log</a></h4>
                <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="{{ count($errors)>0 ? 'icon-minus4':'icon-plus4' }}"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-body collapse {{ count($errors)>0 ? 'in':'out' }}">
                <div class="card-block card-dashboard">
                    <form class="form" method="POST" id="AddContactdetailForm" action="{{ route('communications.store') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-body">

                            <div class="form-group">
                                <label for="name">Communication</label>
                                <textarea class="form-control{{ $errors->has('description') ? ' border-danger' : '' }}" id="description" type="text" placeholder="Description" class="form-control" name="description" value="{{ old('description') }}" required></textarea>
                                @if ($errors->has('description'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </div>
                                @endif
                            </div>

                            
                            <?php $oldValue = old('projects') ? : [] ?>
                            <div class="form-group">
                                <div class="input-group">  
                                    <label for="projectsSelect">Project</label><br>
                                    <select class="form-control{{ $errors->has('projects') ? ' border-danger' : '' }}" id="" name="project" style="width:100%" required>
                                        @foreach($projects as $project)
                                            <option value="{{ $project->id }}" {{!in_array($project->id,$oldValue) && isset($for_project) && $project->id==$for_project->id ? 'selected': ''}} {{ in_array($project->id,$oldValue) ? 'selected' : '' }}>{{ $project->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @if ($errors->has('projects'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('projects') }}</strong>
                                    </div>
                                @endif

                                @if ($errors->has('projects.*'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('projects.*') }}</strong>
                                    </div>
                                @endif
                                
                            </div>
                            <?php $oldValue = old('projects') ? : [] ?>
                            <div class="form-group">
                                <div class="input-group">  
                                    <label for="projectsSelect">Contact Details</label><br>
                                    <select class="form-control{{ $errors->has('contactdetails') ? ' border-danger' : '' }}" id="" name="contactdetail" style="width:100%" required>
                                        @foreach($contactdetails as $contactdetail)
                                            <option value="{{ $contactdetail->id }}" {{ in_array($contactdetail->id,$oldValue) ? 'selected' : '' }}>{{ $contactdetail->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @if ($errors->has('contactdetails'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('contactdetails') }}</strong>
                                    </div>
                                @endif

                                @if ($errors->has('contactdetails.*'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('contactdetails.*') }}</strong>
                                    </div>
                                @endif
                                
                            </div>

                        </div>

                        <div class="form-actions right">
                            <button type="submit" class="btn btn-primary">
                                <i class="icon-check2"></i> Save
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endcan
  

    <div class="card" style="">
        <div class="card-body">

            <div class="card-block">
                <h4 class="card-title">Communication logs  {{isset($for_project)?' for '.$for_project->name:''}}</h4>
             </div>

             <div class="media-list">
                @foreach($communications as $com)

                <div class="media">
                    <a class="media-left" href="#">
                        <i style="font-size: 44px;" class="fa fa-comment-o"></i>
                    </a>
                    <div class="media-body">
                        <div class="row">
                            <div class="col-md-8">
                                    <p class="media-heading" style="font-size:20px">Contact Person: <a href="{{ route('contactdetails.show',$com->contactDetail->id) }}">{{$com->contactDetail->name}}</a></p>
                                    <p class="media-heading" style="font-size:20px">Message: </p>
                                   <p style="text-align:justify; font-size:17px" > {{$com->description}}</p>
                                   @can('update',$com)
                                        <a class="btn btn-outline-warning btn-sm" title="Edit" href="{{ route('communications.edit',$com->id) }}"><i class="icon-edit"></i></a>
                                    @endcan
                               <br>
                            </div>
                            <div class="col-md-4">
                                   
                        <p class="">
                                Project: {{$com->project->name}}<br> 
                                Created By: {{$com->creator->name}}<br>
                                Created On: {{ date('M j, Y', strtotime($com->created_at)) }}<br>
                            </p>
                            </div>
                        </div>
                     
                    </div>
                </div>
                @endforeach
            </div>
           
            <div class="text-xs-center mb-3">
                    <nav aria-label="Page navigation">
                        {{ $communications->links('partials.pagination') }}
                    </nav>
                </div>
        </div>
    </div>
@endsection
