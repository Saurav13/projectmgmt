@extends('layouts.admin')

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />

    <link href="/admin-assets/rangeslider/rangeslider.css" rel="stylesheet" />
    <style>
        .rangeslider--horizontal {
            height: 10px!important;
        }
        .rangeslider__handle {
            width: 20px!important;
            height: 20px!important;
        }
        .rangeslider--horizontal .rangeslider__handle {
            top: -5px!important;
        }
    </style>
@endsection

@section('body')
    @can('create', App\TaskLog::class)
        <div class="card">
            <div class="card-header">
                <h4 class="card-title"><a data-action="collapse">Add New Task Log</a></h4>
                <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="{{ count($errors)>0 ? 'icon-minus4':'icon-plus4' }}"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-body collapse {{ count($errors)>0 ? 'in':'out' }}">
                <div class="card-block card-dashboard">
                    <form class="form" method="POST" id="AddTaskLogForm" action="{{ route('tasklogs.store') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-body">
                            <div class="form-group">
                                <div class="input-group">  
                                    <label for="status">Project</label><br>
                                    <select class="form-control{{ $errors->has('project') ? ' border-danger' : '' }} js-example-basic-multiple" id="chooseProject" name="project" style="width:100%" required>
                                        <option value="" selected disabled>Choose a Project</option>
                                        @foreach($projects as $project)
                                            <option value="{{ $project->id }}" {{ old('project') == $project->id ? 'selected' : '' }}>{{ $project->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @if ($errors->has('project'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('project') }}</strong>
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                <div class="input-group">  
                                    <label for="chooseTask">Task</label><br>
                                    <select class="form-control{{ $errors->has('task') ? ' border-danger' : '' }}" id="chooseTask" name="task" style="width:100%" required>
                                        <option value="" selected disabled>Select a Task</option>
                                    </select>
                                </div>
                                @if ($errors->has('task'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('task') }}</strong>
                                    </div>
                                @endif
                                
                            </div>

                            <div class="form-group">
                                <label for="name">Log detail</label>
                                <textarea class="form-control{{ $errors->has('comments') ? ' border-danger' : '' }}" id="comments" type="text" placeholder="Comments" class="form-control" name="comments" required>{{ old('comments') }}</textarea>
                                @if ($errors->has('comments'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('comments') }}</strong>
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                <div class="input-group">   
                                    <label for="percent_completed">Percent Completed <span id="progress_rate">{{ old('percent_completed') ? : 0 }}</span>%</label>
                                    <input style="border: none !important; position: absolute; width: 1px; height: 1px; overflow: hidden; opacity: 0;z-index: 0;" type="range" min="0" max="100" step="1" class="form-control{{ $errors->has('percent_completed') ? ' border-danger' : '' }}" id="percent_completed" name="percent_completed" value="{{ old('percent_completed') ? : 0 }}" placeholder="Percent Completed" required>
                                    
                                </div>
                                @if ($errors->has('percent_completed'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('percent_completed') }}</strong>
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                <div class="input-group">   
                                    <label for="working_hours">Working Hours</label>
                                    <input type="number" min="0" step="0.01" class="form-control{{ $errors->has('working_hours') ? ' border-danger' : '' }}" id="working_hours" name="working_hours" value="{{ old('working_hours') }}" placeholder="Working Hours (eg: 1.25)" required>
                                </div>
                                @if ($errors->has('working_hours'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('working_hours') }}</strong>
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                <div class="input-group">   
                                    <label for="files">Files (Optional)</label>
                                    <input type="file" class="form-control{{ $errors->has('files') ? ' border-danger' : '' }}" id="files" name="files[]" placeholder="Upload Files" multiple>
                                </div>
                                @if ($errors->has('files'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('files') }}</strong>
                                    </div>
                                @endif
                            </div>
                            
                            <div class="form-group">
                                <div class="input-group">  
                                    <label for="status">Status</label><br>
                                    <select class="form-control{{ $errors->has('status') ? ' border-danger' : '' }} js-example-basic-multiple" id="status" name="status" required>
                                        <option value="" selected disabled>Choose a Status</option>
                                        @foreach($statuses as $status)
                                            <option value="{{ $status->id }}">{{ $status->status }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @if ($errors->has('status'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('status') }}</strong>
                                    </div>
                                @endif
                            </div>


                        </div>

                        <div class="form-actions right">
                            <button type="submit" class="btn btn-primary">
                                <i class="icon-check2"></i> Save
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endcan

    <div class="card" style="height: 497px;">
        <div class="card-body">
            <div class="card-block">
                <h4 class="card-title">Task logs  {{isset($for_task)?' for '.$for_task->title:''}}</h4>
             </div>
            <ul class="list-group list-group-flush">
                @foreach($tasklogs as $com)
                    <li class="list-group-item">
                        <p class="pull-right">
                            Task: {{$com->task->title}}<br>
                            Project: {{$com->task->project->name }}<br>
                            Created By: {{$com->creator->name}}<br>
                            Created On: {{ date('M j, Y', strtotime($com->created_at)) }}<br>
                        </p>

                        <p>{{$com->comments}}</p>

                        <p>Completed: {{ $com->percent_completed ? : '-' }} %</p>                        

                        <p>Working Hours: {{ $com->working_hours ? : '-' }}</p>                        

                        @if($com->files->count() > 0)
                            <br>
                            <span>Files: </span><br>

                            @foreach($com->files as $file)
                                <a href="{{ route('tasks.getFile',$file->filepath) }}" target="_blank">{{ $file->filepath }}</a><br>
                            @endforeach
                        @endif
                        <br>
                        <p>Status: <span class="tag {{ ($com->status == 7 ? 'tag-success' : 'tag-primary') }}"> {{ $com->state->status }}</span></p>                        
                    
                        <?php $verification = $com->verification ?>
                        @if($verification)
                            
                            <p><strong>Verification:</strong></p>
                            <p>Comment: {{ $verification->comment }}</p>
                            <p>Status: <span class="tag {{ $verification->status ? 'tag-success' : 'tag-danger' }}"> {{ $verification->status ? 'Verified' : 'Rejected' }}</span></p></p>
                        @endif
                    </li>
                @endforeach
            </ul>
            <div class="text-xs-center mb-3">
                <nav aria-label="Page navigation">
                    {{ $tasklogs->links('partials.pagination') }}
                </nav>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>

    <script src="/admin-assets/rangeslider/rangeslider.min.js"></script>
    <script>
        
        $(function () {

            $('#chooseProject').select2({
                'placeholder': 'Select Project'
            });

            var project = $('#chooseProject').val();
            getTasks(project,"{{ old('task') }}");

            $('#chooseProject').on('change',function(){
                var project = $('#chooseProject').val();

                getTasks(project,null);
            });

            function getTasks(project,select) {
                
                if(!project) return;

                $.ajax({
                    url: "/projects/"+project+"/getTasks",
                    method: 'GET',
                    success: function(result){
                                                             
                        var html = '<option value="" selected disabled>Select a Task</option>';
                        for(var i=0;i<result.length;i++){
                            html += '<option value="'+result[i].id+'" '+ (result[i].id == select ? 'selected' : '')+'>'+result[i].title+'</option>';
                        }
                        $('#chooseTask').html(html);

                        $('#chooseTask').select2({
                            'placeholder': 'Select a Task'
                        });
                    },
                    error: function(result){
                        swal(
                            'Oops...',
                            'Something went wrong!',
                            'error'
                        )
                    }
                });
            }

            var progressBarInput = $('input[type="range"]');

            progressBarInput.rangeslider({

                // Feature detection the default is `true`.
                // Set this to `false` if you want to use
                // the polyfill also in Browsers which support
                // the native <input type="range"> element.
                polyfill: false,

                // Default CSS classes
                rangeClass: 'rangeslider',
                disabledClass: 'rangeslider--disabled',
                horizontalClass: 'rangeslider--horizontal',
                verticalClass: 'rangeslider--vertical',
                fillClass: 'rangeslider__fill',
                handleClass: 'rangeslider__handle',

                // Callback function
                onInit: function() {

                },

                // Callback function
                onSlide: function(position, value) {

                    $("#progress_rate").html(value);
                },

                // Callback function
                onSlideEnd: function(position, value) {

                    
                }
            });
        });

    </script>
@endsection