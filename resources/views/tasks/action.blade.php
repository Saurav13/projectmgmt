{{-- <a class="btn btn-outline-info btn-sm" title="View" href="{{ route('tasks.show',$task->id) }}"><i class="icon-eye"></i></a> --}}
@can('update',$task)
    @if($task->status != 7)
        <a class="btn btn-outline-warning btn-sm" title="Edit" href="{{ route('tasks.edit',$task->id) }}"><i class="icon-edit"></i></a>
    @endif
@endcan

@can('viewlog',$task)
    <a class="btn btn-outline-primary btn-sm" title="View Logs" href="/tasks/listlogs/?id={{$task->id}}"><i class="icon-drag"></i></a>
@endcan

@can('destroy',$task)
    <form action="{{ route('tasks.destroy',$task->id) }}" method="POST" style="display:inline">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="DELETE" >

        <button title="delete" type="button" class="btn btn-outline-danger btn-sm deleteButton"><i class="icon-trash-o"></i></button>
    </form>
@endcan