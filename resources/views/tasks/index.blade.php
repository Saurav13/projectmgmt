@extends('layouts.admin')

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
@endsection

@section('body')
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
            <h2 class="content-header-title">Tasks</h2>
        </div>
        <div class="text-xs-right">
            <a href="{{ route('tasks.kanban') }}" class="btn btn-primary btn-min-width mr-2 mb-1">Switch to Kanban View</a>
        </div>
    </div>
    <div class="content-body">
        @can('create',App\Task::class)
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title"><a data-action="collapse">Add New Task</a></h4>
                    <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="{{ count($errors)>0 ? 'icon-minus4':'icon-plus4' }}"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-body collapse {{ count($errors)>0 ? 'in':'out' }}">
                    <div class="card-block card-dashboard">
                        <form class="form" method="POST" id="AddTaskForm" action="{{ route('tasks.store') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-body">
                                <h4 class="form-section"><i class="icon-eye6"></i> About Task</h4>
                                
                                <div class="form-group">
                                    <div class="input-group">  
                                        <label for="status">Project</label><br>
                                        <select class="form-control{{ $errors->has('project') ? ' border-danger' : '' }} js-example-basic-multiple" id="chooseProject" name="project"  required>
                                            <option value="" selected disabled>Choose a Project</option>
                                            @foreach($projects as $project)
                                                <option value="{{ $project->id }}" {{ old('project') == $project->id ? 'selected' : '' }}>{{ $project->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @if ($errors->has('project'))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ $errors->first('project') }}</strong>
                                        </div>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <div class="input-group">  
                                        <label for="chooseTeam">Team</label><br>
                                        <select id="chooseTeam" class="form-control{{ $errors->has('status') ? ' border-danger' : '' }} js-example-basic-multiple" name="team" required>
                                            <option value="" id="selectteam" projects='[]' selected disabled>Choose a Team</option>
                                            @foreach($teams as $team)
                                                <?php $project_ids = $team->projects->pluck('id')->toArray() ?>
                                                <option value="{{ $team->id }}" {{ old('team') == $team->id ? 'selected' : '' }} projects="{{ json_encode($project_ids) }}" {{ in_array(old('project'), $project_ids) ? '' : 'hidden' }}>{{ $team->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @if ($errors->has('team'))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ $errors->first('team') }}</strong>
                                        </div>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input class="form-control{{ $errors->has('name') ? ' border-danger' : '' }}" id="name" type="text" placeholder="Name" class="form-control" name="name" value="{{ old('name') }}" required>
    
                                    @if ($errors->has('name'))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </div>
                                    @endif
                                </div>
    
                                <div class="form-group">
                                    <label for="description">Description</label>
                                    <textarea rows="5" class="form-control{{ $errors->has('description') ? ' border-danger' : '' }}" placeholder="Description"  name="description" required>{{ old('description') }}</textarea>
    
                                    @if ($errors->has('description'))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ $errors->first('description') }}</strong>
                                        </div>
                                    @endif
                                </div>
    
                                <div class="form-group">
                                    <div class="input-group">   
                                        <label for="priority">Priority</label>
                                        <input type="number" min="0" class="form-control{{ $errors->has('priority') ? ' border-danger' : '' }}" id="priority" name="priority" value="{{ old('priority') }}" placeholder="Priority" required>
                                    </div>
                                    @if ($errors->has('priority'))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ $errors->first('priority') }}</strong>
                                        </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <div class="input-group">   
                                        <label for="estimated_minutes">Estimated Hours</label>
                                        <input type="number" min="0" step="0.01" class="form-control{{ $errors->has('estimated_minutes') ? ' border-danger' : '' }}" id="estimated_minutes" name="estimated_minutes" value="{{ old('estimated_minutes') }}" placeholder="Estimated Hours (eg: 1.25)" required>
                                    </div>
                                    @if ($errors->has('estimated_minutes'))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ $errors->first('estimated_minutes') }}</strong>
                                        </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <div class="input-group">   
                                        <label for="deadline">Deadline</label>
                                        <input type="date" id="deadline" class="form-control{{ $errors->has('estimated_minutes') ? ' border-danger' : '' }}" id="deadline" value="{{ old('deadline') }}" required name="deadline" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Deadline" data-original-title="" title="">
                                    </div>
                                    @if ($errors->has('deadline'))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ $errors->first('deadline') }}</strong>
                                        </div>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <div class="input-group">  
                                        <label for="status">Status</label><br>
                                        <select class="form-control{{ $errors->has('status') ? ' border-danger' : '' }} js-example-basic-multiple" id="status" name="status" required>
                                            @foreach($statuses as $status)
                                                <option value="{{ $status->id }}" {{ $status->id == 4 ? 'selected' : '' }}>{{ $status->status }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @if ($errors->has('status'))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ $errors->first('status') }}</strong>
                                        </div>
                                    @endif
                                </div>
                            </div>
    
                            <div class="form-actions right">
                                <button type="submit" class="btn btn-primary">
                                    <i class="icon-check2"></i> Save
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        @endcan

        <div class="card">
            <div class="card-body collapse in">
                <div class="card-block card-dashboard dataTable">
                    <h4 class="card-title">My Tasks</h4>
                        
                    {!! $mytasks->html()->table(['id' => 'mytasks'])  !!}
                    
                </div>
            </div>
        </div>
        
        <div class="card">
            <div class="card-body collapse in">
                <div class="card-block card-dashboard dataTable">
                    <h4 class="card-title">Team Tasks</h4>

                    {!! $tasks->html()->table(['id' => 'tasks'])  !!}
                </div>
            </div>
        </div>
   </div>
@stop

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
    <script>
        $(document).ready(function() {

            $('#chooseProject').on('change',function(){
                var project = parseInt($('#chooseProject').val());
                $("#chooseTeam option").attr('hidden','hidden');
                $('#chooseTeam option').each(function() {
                    var prjs = JSON.parse($(this).attr('projects'));
                    if(prjs.indexOf(project) != -1) $(this).removeAttr('hidden');

                });
                
                $("#chooseTeam").val('');
                $('#selectteam').removeAttr('hidden');
            });
        });
    </script>

    {!! $mytasks->html()->scripts() !!}
    {!! $tasks->html()->scripts() !!}
@endsection