@extends('layouts.admin')

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
@endsection

@section('body')
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
            <h2 class="content-header-title">Edit Task</h2>
        </div>
        <div class="text-xs-right">
            <a href="{{ route('tasks.index') }}" class="btn btn-primary btn-min-width mr-2 mb-1">Back</a>
        </div>
    </div>
    <div class="content-body">
        <div class="card">
            <div class="card-body collapse in">
                <div class="card-block card-dashboard">
                    <form class="form" method="POST" action="{{ route('tasks.update',$task->id) }}">
                        {{ csrf_field() }}
                        <input name="_method" value="PATCH" hidden />
                        
                        <div class="form-body">
                            <h4 class="form-section"><i class="icon-eye6"></i> About Task</h4>

                            <div class="form-group">
                                <div class="input-group">  
                                    <label for="status">Project</label><br>
                                    <select class="form-control{{ $errors->has('status') ? ' border-danger' : '' }} js-example-basic-multiple" id="chooseProject" name="project"  required>
                                        <option value="" selected disabled>Choose a Project</option>
                                        @foreach($projects as $project)
                                            <option value="{{ $project->id }}" {{ $task->project_id == $project->id ? 'selected' : '' }}>{{ $project->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @if ($errors->has('project'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('project') }}</strong>
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                <div class="input-group">  
                                    <label for="chooseTeam">Team</label><br>
                                    <select id="chooseTeam" class="form-control{{ $errors->has('status') ? ' border-danger' : '' }} js-example-basic-multiple" name="team" required>
                                        <option value="" id="selectteam" projects='[]' selected disabled>Choose a Team</option>
                                        @foreach($teams as $team)
                                            <?php $project_ids = $team->projects->pluck('id')->toArray() ?>
                                            <option value="{{ $team->id }}" {{ $task->team_id == $team->id ? 'selected' : '' }} projects="{{ json_encode($project_ids) }}" {{ in_array($task->project_id, $project_ids) ? '' : 'hidden' }}>{{ $team->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @if ($errors->has('team'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('team') }}</strong>
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="name">Name</label>
                                <input class="form-control{{ $errors->has('name') ? ' border-danger' : '' }}" id="name" type="text" placeholder="Name" class="form-control" name="name" value="{{ $task->title }}" required>

                                @if ($errors->has('name'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea rows="5" class="form-control{{ $errors->has('description') ? ' border-danger' : '' }}" placeholder="Description"  name="description" required>{{ $task->description }}</textarea>

                                @if ($errors->has('description'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                <div class="input-group">   
                                    <label for="priority">Priority</label>
                                    <input type="number" min="0" class="form-control{{ $errors->has('priority') ? ' border-danger' : '' }}" id="priority" name="priority" value="{{ $task->priority }}" placeholder="Priority" required>
                                </div>
                                @if ($errors->has('priority'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('priority') }}</strong>
                                    </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <div class="input-group">   
                                    <label for="estimated_minutes">Estimated Hours</label>
                                    <input type="number" min="0" step="0.01" class="form-control{{ $errors->has('estimated_minutes') ? ' border-danger' : '' }}" id="estimated_minutes" name="estimated_minutes" value="{{ $task->estimated_minutes }}" placeholder="Estimated Hours (eg: 1.25)" required>
                                </div>
                                @if ($errors->has('estimated_minutes'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('estimated_minutes') }}</strong>
                                    </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <div class="input-group">   
                                    <label for="deadline">Deadline</label>
                                    <input type="date" id="deadline" class="form-control{{ $errors->has('estimated_minutes') ? ' border-danger' : '' }}" id="deadline" value="{{ $task->deadline }}" required name="deadline" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Deadline" data-original-title="" title="">
                                </div>
                                @if ($errors->has('deadline'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('deadline') }}</strong>
                                    </div>
                                @endif
                            </div>
                            
                            {{-- <div class="form-group">
                                <div class="input-group">  
                                    <label for="status">Status</label><br>
                                    <select class="form-control{{ $errors->has('status') ? ' border-danger' : '' }} js-example-basic-multiple" id="status" name="status" required>
                                        <option value="" selected disabled>Choose a Status</option>
                                        @foreach($statuses as $status)
                                            <option value="{{ $status->id }}" {{ $task->status == $status->id ? 'selected' : '' }}>{{ $status->status }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @if ($errors->has('status'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('status') }}</strong>
                                    </div>
                                @endif
                            </div> --}}
                        </div>

                        <div class="form-actions right">
                            <button type="submit" class="btn btn-success">
                                <i class="icon-check2"></i> Save
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
   </div>
@stop

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
    <script>
        $(document).ready(function() {

            $('#chooseProject').on('change',function(){
                var project = parseInt($('#chooseProject').val());
                $("#chooseTeam option").attr('hidden','hidden');
                $('#chooseTeam option').each(function() {
                    var prjs = JSON.parse($(this).attr('projects'));
                    if(prjs.indexOf(project) != -1) $(this).removeAttr('hidden');

                });
                
                $("#chooseTeam").val('');
                $('#selectteam').removeAttr('hidden');
            });
        });

    </script>
@endsection