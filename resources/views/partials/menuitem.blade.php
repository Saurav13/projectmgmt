@foreach ($menu_items as $item)
    @if(!$item->user_can_access) @continue @endif

    @if($menuList->where('parent_id',$item->id)->count() > 0) 
        <li class="nav-item has-sub"><a href="{{ $item->link }}"><span class="menu-title">{{ $item->name }}</span></a>

        <ul class="menu-content" style="">
            @include('partials.menuitem',[ 'menu_items' => $menuList->where('parent_id',$item->id) ])
        </ul>
    @else
        <li class="nav-item {{ Request::is($item->link.'*') ? 'active' : '' }}"><a href="{{ URL::to($item->link) }}"><span class="menu-title">{{ $item->name }}</span></a>
        </li>
    @endif
@endforeach