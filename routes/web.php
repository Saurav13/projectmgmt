<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
  
Route::get('profile','ProfileController@index')->name('profile');
Route::post('profile/changePassword','ProfileController@changePassword')->name('profile.changePassword');
Route::post('profile/changeName','ProfileController@updateName')->name('profile.updateName');

Route::get('contact-us-messages/unseen','ContactUsMessageController@unseenMsg')->name('contact-us-messages.unseen');    
Route::get('contact-us-messages/reply/{id}','ContactUsMessageController@reply')->name('contact-us-messages.reply');
Route::post('contact-us-messages/reply/{id}','ContactUsMessageController@replySend')->name('contact-us-messages.replySend');
Route::resource('contact-us-messages', 'ContactUsMessageController',['only' => ['show','destroy','index']]);
Route::get('getUnseenMsgCount','ContactUsMessageController@getUnseenMsgCount')->name('getUnseenMsgCount');
Route::get('getUnseenMsg','ContactUsMessageController@getUnseenMsg')->name('getUnseenMsg');

Route::resource('users', 'UserController',['except' => ['create','show']]);
Route::resource('roles', 'RoleController',['except' => ['show']]);
Route::resource('teams', 'TeamController',['except' => ['show']]);

Route::resource('/project/communications','CommunicationController',['except' => ['create']]);
Route::get('/project/listcommunications','CommunicationController@listCommunicationByProject');
Route::resource('/project/projectlogs','ProjectLogController',['except' => ['create','show']]);
Route::get('/project/listlogs','ProjectLogController@listLogsByProject');
Route::get('storage/projectfiles/{filename}', 'ProjectLogController@getFile')->name('projects.getFile');

Route::get('/projects/kanban','ProjectController@kanban')->name('projects.kanban');
Route::post('/projects/updateStatus','ProjectController@updateStatus');
Route::resource('projects', 'ProjectController',['except' => ['create','show']]);
Route::resource('/project/contactdetails','ContactDetailsController',['except' => ['create']]);

Route::get('projects/{id}/getTasks', 'ProjectController@getTasks');

Route::get('/tasks/kanban','TaskController@kanban')->name('tasks.kanban');
Route::post('/tasks/updateStatus','TaskController@updateStatus');

Route::get('storage/taskfiles/{filename}', 'TaskLogController@getFile')->name('tasks.getFile');
Route::resource('tasklogs', 'TaskLogController',['except' => ['create','show']]);
Route::get('/tasks/listlogs','TaskLogController@listLogsByTask');

Route::get('tasks/{id}/getMembers', 'TaskAssignController@getTeamMembers');
Route::resource('task/assignments', 'TaskAssignController',['except' => ['create','show','edit','update']]);

Route::get('task/verifications', 'TaskVerificationController@index')->name('verifications.index');
// Route::get('tasks/{id}/verify', 'TaskVerificationController@create')->name('verifications.create');
Route::post('task/verify', 'TaskVerificationController@store')->name('verifications.store');

Route::resource('tasks', 'TaskController',['except' => ['show']]);

Route::post('/menus/updatePriority','MenuController@updatePriority');
Route::resource('menus', 'MenuController',['except' => ['create','show']]);

Route::get('/dashboard','AdminController@index');
Route::get('/','AdminController@index');

Route::get('/assets/{source}/{img}/{h}/{w}',function($source,$img, $h, $w){
    return \Image::make(public_path("/".$source."/".$img))->resize($h, $w)->response('jpg');
})->name('asset');
Route::get('/assets/{source}/{source2}/{img}/{h}/{w}',function($source,$source2,$img, $h, $w){
   
    return \Image::make(public_path("/".$source."/".$source2."/".$img))->resize($h, $w)->response('jpg');
})->name('asset1');
Route::get('/asset/{source}/{img}/{ext}/{h}/{w}',function($source,$img,$ext, $h, $w){
    return \Image::make(public_path("/".$source."/".$img))->resize($h, $w)->response($ext);
})->name('asset2');

