<?php

use Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('statuses')->insert([
            [
                'description' => 'Prospect',
                'status' => 'Prospect',
                'modal' => 'Project',
            ],
            [
                'description' => 'Confirmed',
                'status' => 'Confirmed',
                'modal' => 'Project',
            ],
            [
                'description' => 'Cancelled',
                'status' => 'Cancelled',
                'modal' => 'Project',
            ],
            [
                'description' => 'New',
                'status' => 'New',
                'modal' => 'Task',
            ],

            [
                'description' => 'Active',
                'status' => 'Active',
                'modal' => 'Task',
            ],
            [
                'description' => 'Closed',
                'status' => 'Closed',
                'modal' => 'Task',
            ],
            [
                'description' => 'Resolved',
                'status' => 'Resolved',
                'modal' => 'Task',
            ],
        ]);
    }
}
