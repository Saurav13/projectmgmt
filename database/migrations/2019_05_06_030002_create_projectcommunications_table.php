<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectcommunicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_communications', function (Blueprint $table) {
            $table->increments('id');
            $table->text('description');

            $table->integer('project_id')->unsigned()->nullable();
            $table->integer('contactdetail_id')->unsigned()->nullable();


            $table->foreign('contactdetail_id')->references('id')->on('contact_details')->onDelete('set null');
            $table->foreign('project_id')->references('id')->on('projects')->onDelete('set null');

            $table->integer('created_by')->unsigned()->nullable();

            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
            $table->timestamps();
            $table->softDeletes();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projectcommunications');
    }
}
