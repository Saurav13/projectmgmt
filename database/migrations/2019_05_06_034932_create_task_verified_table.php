<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskVerifiedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('task_verifications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tasklog_id')->unsigned()->nullable();
            $table->foreign('tasklog_id')->references('id')->on('task_logs')->onDelete('set null');
            $table->text('comment');

            $table->boolean('status')->default(1);

            $table->integer('verified_by')->unsigned()->nullable();
            $table->foreign('verified_by')->references('id')->on('users')->onDelete('set null');

          


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('task_verified');
    }
}
