<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('description');
            $table->integer('priority')->default(0);
            $table->integer('estimated_minutes');
            $table->string('deadline');
            $table->string('started_date')->nullable();
            $table->string('percent_completed')->default(0);
            $table->string('completed_date')->nullable();


            $table->integer('status')->unsigned();


            $table->integer('project_id')->unsigned()->nullable();
            $table->integer('team_id')->unsigned()->nullable();

            $table->foreign('project_id')->references('id')->on('projects')->onDelete('set null');
            $table->foreign('team_id')->references('id')->on('teams')->onDelete('set null');
            
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
