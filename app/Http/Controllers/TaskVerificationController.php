<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TaskLog;
use App\Task;
use App\Services\TaskLogService;
use App\TaskVerification;
use Auth;

class TaskVerificationController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('view',TaskVerification::class);

        // $tasklogs = TaskLog::orderBy('created_at','desc')->paginate(10);
        $tasklogs = TaskLogService::tasklogs();

        return view('task_verifications.index',compact('tasklogs','statuses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $tasklog = TaskLog::findOrFail($id);
        $this->authorize('taskverify',$tasklog);

        return view('task_verifications.create',compact('tasklog'));
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tasklog = TaskLog::findOrFail($request->id);
        $this->authorize('taskverify',$tasklog);

        $request->validate([
            'id' => 'required|exists:task_logs',
            'comment' => 'required',
            'status' => 'required|in:1,0',
        ],[
            'status.in' => 'Status can be either verified or rejected'
        ]);

        $verify = new TaskVerification;
        $verify->comment = $request->comment;
        $verify->status = $request->status;
        $verify->tasklog_id = $tasklog->id;
        $verify->verified_by = Auth::user()->id;
        $verify->save();

        if($verify->status){
            // Task::where('id',$tasklog->task_id)->update(['status'=>$tasklog->status,'percent_completed'=>$tasklog->percent_completed]);
            Task::where('id',$tasklog->task_id)->update(['status'=>7,'percent_completed'=>$tasklog->percent_completed]);

            // if($tasklog->status == 7)
                Task::where('id',$tasklog->task_id)->update(['completed_date'=>$tasklog->updated_at]);
            $tasklog->status = 7;
            $tasklog->save();
        }

        $request->session()->flash('success', 'Task successfully verified');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
