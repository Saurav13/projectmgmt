<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataTables\ProjectsDataTable;
use App\Services\ProjectService;
use App\Services\TaskService;
use App\Project;
use App\Team;
use App\Status;
use Auth;
use Illuminate\Support\Collection;

class ProjectController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('view',Project::class);

        $dataTable = new ProjectsDataTable;
        $statuses = Status::where('modal','Project')->get();
        $teams = Team::get();

        return $dataTable->render('projects.index',compact('statuses','teams'));
    }

    public function getTasks($id,Request $request){
        $project = Project::findOrFail($id);

        $tasks = TaskService::tasks()->where('project_id',$project->id)->toArray();

        return response()->json(array_values($tasks), 200);
    }

    public function kanban(){
        $this->authorize('view',Project::class);

        $statuses = Status::where('modal','Project')->get();
        $projects= ProjectService::projects();
       

        // dd($projects);

        return view('projects.kanban',compact('statuses','projects'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create',Project::class);
        
        $request->validate([
            'name' => 'required|string|max:191',
            'description' => 'required',
            'company' => 'required|string|max:191',
            'status' => 'required|exists:statuses,id',
            'teams' => 'required|min:1',
            'teams.*' => 'required|exists:teams,id',
        ],[
            'teams.*.exists' => 'Team Assignment Failed'
        ]);

        $project = new Project;
        $project->name = $request->name;
        $project->description = $request->description;
        $project->company = $request->company;
        $project->status = $request->status;
        $project->created_by = Auth::user()->id;
        $project->updated_by = Auth::user()->id;

        $project->save();

        $project->teams()->attach($request->teams);

        $request->session()->flash('success', 'Project Added Successfully');

        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('update',$project = Project::findOrFail($id));
        
        
        $statuses = Status::get();
        $teams = Team::get();

        return view('projects.edit',compact('project','statuses','teams'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('update', $project = Project::findOrFail($id));
        
        $request->validate([
            'name' => 'required|string|max:191',
            'description' => 'required',
            'company' => 'required|string|max:191',
            'teams' => 'required|min:1',
            'teams.*' => 'required|exists:teams,id',
        ],[
            'teams.*.exists' => 'Team Assignment Failed'
        ]);

              
        $project->name = $request->name;
        $project->description = $request->description;
        $project->company = $request->company;
        $project->updated_by = Auth::user()->id;

        $project->save();

        $project->teams()->sync($request->teams);

        $request->session()->flash('success', 'Project Updated Successfully');

        return redirect()->route('projects.index');        
    }

    public function updateStatus(Request $request){
        $this->authorize('updateStatus', $project = Project::findOrFail($request->project_id));
        
        $request->validate([
            'project_id' => 'required|exists:projects,id',
            'status_id' => 'required|exists:statuses,id',
            'description' => 'required',            
        ]);

         
        $project->status = $request->status_id;
        $project->save();

        $project->logs()->create([
            'description' => $request->description,
            'status' => $request->status_id,
            'created_by' => Auth::user()->id,
        ]);

        return response()->json('status updated', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $this->authorize('destroy',$project = Project::findOrFail($id));
        
        
        $project->delete();
        
        $request->session()->flash('success', 'Project Deleted Successfully');

        return redirect()->back();
    }
}
