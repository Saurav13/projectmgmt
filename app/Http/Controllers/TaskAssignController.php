<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataTables\TaskAssignsDataTable;
use App\Services\ProjectService;
use App\TaskAssign;
use App\Task;
use Carbon\Carbon;
use Auth;

class TaskAssignController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(Request $request)
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $this->authorize('view',TaskAssign::class);

        $dataTable = new TaskAssignsDataTable;

        // $tasks = Task::where('status','!=',7)->get();
        $projects = ProjectService::projects();

        return $dataTable->render('task_assigns.index',compact('projects'));
    }

    public function getTeamMembers($id){
        $task = Task::findOrFail($id);
        return $task->team->members;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $task = Task::find($request->task_id);

        $this->authorize('taskassign',$task);
        
        $request->validate([
            'task_id' => 'required|exists:tasks,id',
            'assigned_to' => 'required|exists:users,id'

        ],[
            'task.exists' => 'Task Assignment Failed',
            'assigned_to.exists' => 'User Assignment Failed',
        ]);

        
        if(!$task->team->members->pluck('id')->contains($request->assigned_to)){
            $request->session()->flash('invalid_member', 'Invalid Team Member.');

            return redirect()->back()->withInput();
        }

        $prev_task_assigns = TaskAssign::where('task_id',$request->task_id)->get();
        if($prev_task_assigns->count() > 0){
            foreach($prev_task_assigns as $prev_task_assign){
                $prev_task_assign->status = 0;
                $prev_task_assign->save();
            }
        }

        $task_assign = new TaskAssign;
        $task_assign->task_id = $request->task_id;
        $task_assign->status=1;

        $task_assign->assigned_to = $request->assigned_to;
        $task_assign->assigned_by = Auth::user()->id;

        $task_assign->save();

        $request->session()->flash('success', 'TaskAssign Added Successfully');
        $task->started_date = Carbon::now();
        $task->save();

        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function edit($id)
    // {
    //     $this->authorize('update',TaskAssign::class);

    //     $tasks = Task::get();
    //     $task_assign = TaskAssign::findOrFail($id);

    //     return view('task_assigns.edit',compact('task_assign','tasks'));
    // }

    // /**
    //  * Update the specified resource in storage.
    //  *
    //  * @param  \Illuminate\Http\Request  $request
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function update(Request $request, $id)
    // {
    //     $this->authorize('update',TaskAssign::class);
        
    //     $request->validate([
    //         'task_id' => 'required|exists:tasks,id',
    //         'assigned_to' => 'required|exists:users,id'

    //     ],[
    //         'task.exists' => 'Task Assignment Failed',
    //         'assigned_to.exists' => 'User Assignment Failed',
    //     ]);

    //     $task = Task::find($request->task_id);
        
    //     if(!$task->team->members->pluck('id')->contains($request->assigned_to)){
    //         $request->session()->flash('invalid_member', 'Invalid Team Member.');

    //         return redirect()->back()->withInput();
    //     }

    //     $task_assign = TaskAssign::findOrFail($id);
        
    //     $task_assign->task_id = $request->task_id;
    //     $task_assign->assigned_to = $request->assigned_to;
    //     $task_assign->assigned_by = Auth::user()->id;

    //     $task_assign->save();

    //     $request->session()->flash('success', 'TaskAssign Updated Successfully');

    //     return redirect()->route('task_assigns.index');
    // }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $this->authorize('destroy',TaskAssign::class);
        
        $task_assign = TaskAssign::findOrFail($id);
        $task_assign->delete();
        
        $request->session()->flash('success', 'TaskAssign Deleted Successfully');

        return redirect()->back();
    }
}
