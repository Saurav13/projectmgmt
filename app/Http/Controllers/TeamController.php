<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataTables\TeamsDataTable;
use App\Team;
use App\User;
use App\Role;
use Auth;

class TeamController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd("asd");
        $this->authorize('view',Team::class);

        $dataTable = new TeamsDataTable;

        return $dataTable->render('teams.index');
    }

    public function create()
    {
        $this->authorize('create',Team::class);
        
        $users = User::orderBy('name')->get();
        $roles = Role::get();

        return view('teams.create',compact('users','roles'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create',Team::class);
        
        $request->validate([
            'name' => 'required|string|max:191',
            'status' => 'required|in:1,0',
            'members' => 'required|min:1',
            'members.*.user_id' => 'required|exists:users,id',
            'members.*.role_id' => 'required|exists:roles,id',
        ],[
            'members.*.user_id.required' => 'User Assignment Failed. All the member fields are required.',
            'members.*.user_id.exists' => 'User Assignment Failed. All the members must be valid users.',
            'members.*.role_id.required' => 'Role Assignment Faied. Please make sure all users have a valid role.',
            'members.*.role_id.exists' => 'Role Assignment Faied. Please make sure all users have a valid role.',
            'status.in' => 'Status can be either active or inactive.'
        ]);

        $collection = collect($request->members);
        if($collection->unique('user_id')->count() != $collection->count()){
            $request->session()->flash('duplicate', 'We found some duplicate entries of Members. Please recheck and try again.');
            return redirect()->back()->withInput();
        }

        $team = new Team;
        $team->name = $request->name;
        $team->active = $request->status;
        $team->created_by = Auth::user()->id;
        $team->updated_by = Auth::user()->id;

        $team->save();

        $team->members()->attach($request->members);

        $request->session()->flash('success', 'Team Added Successfully');

        return redirect()->route('teams.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('update',Team::class);
        
        $team = Team::findOrFail($id);
        $users = User::orderBy('name')->get();
        $roles = Role::get();

        return view('teams.edit',compact('team','users','roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('update',Team::class);
// dd($request->all());
        $request->validate([
            'name' => 'required|string|max:191',
            'status' => 'required|in:1,0',
            'members' => 'required|min:1',
            'members.*.user_id' => 'required|exists:users,id',
            'members.*.role_id' => 'required|exists:roles,id',
        ],[
            'members.*.user_id.required' => 'User Assignment Failed. All the member fields are required.',
            'members.*.user_id.exists' => 'User Assignment Failed. All the members must be valid users.',
            'members.*.role_id.required' => 'Role Assignment Faied. Please make sure all users have a valid role.',
            'members.*.role_id.exists' => 'Role Assignment Faied. Please make sure all users have a valid role.',
            'status.in' => 'Status can be either active or inactive.'
        ]);

        $collection = collect($request->members);
        if($collection->unique('user_id')->count() != $collection->count()){
            $request->session()->flash('duplicate', 'We found some duplicate entries of Members. Please recheck and try again.');
            return redirect()->back()->withInput();
        }

        $team = Team::findOrFail($id);
        $team->name = $request->name;
        $team->active = $request->status;
        $team->updated_by = Auth::user()->id;

        $team->save();

        $team->members()->detach();
        $team->members()->attach($request->members);

        $request->session()->flash('success', 'Team Updated Successfully');

        return redirect()->route('teams.index');        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $this->authorize('destroy',Team::class);
        
        $team = Team::findOrFail($id);
        $team->delete();
        
        $request->session()->flash('success', 'Team Deleted Successfully');

        return redirect()->route('teams.index');        
    }
}
