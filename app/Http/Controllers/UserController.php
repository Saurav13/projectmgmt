<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataTables\UsersDataTable;
use App\User;
use App\Role;
use App\Mail\NewUserAdded;
use Mail;

class UserController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        // $this->authorizeResource(User::class, 'user');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('view',User::class);

        $dataTable = new UsersDataTable;
        $roles = Role::get();

        return $dataTable->render('users.index',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create',User::class);
        
        $request->validate([
            'name' => 'required|string|max:191',
            'email' => 'required|string|email|max:191|unique:users',
            'password' => 'required|string|min:6',
            'roles' => 'required|min:1',
            'roles.*' => 'required|exists:roles,id',
        ],[
            'roles.*.exists' => 'Role Assignment Failed'
        ]);

        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);

        $user->assignRole($request->roles);

        $user->save();

        Mail::to($user->email)->send(new NewUserAdded($user,$request->password));

        $request->session()->flash('success', 'User Added Successfully');

        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('update',User::class);
        
        $user = User::findOrFail($id);
        $roles = Role::get();

        return view('users.edit',compact('user','roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('update',User::class);
        
        $user = User::findOrFail($id);

        if($request->input('email') == $user->email){
            $request->validate([
                'name' => 'required|string|max:191',
                'password' => 'nullable|string|min:6',
                'status' => 'required|string|in:Active,Inactive',
                'roles' => 'required|min:1',
                'roles.*' => 'required|exists:roles,id',
            ],[
                'roles.*.exists' => 'Role Assignment Failed'
            ]);
        }
        else{
            $request->validate([
                'name' => 'required|string|max:191',
                'email' => 'required|string|email|max:191|unique:users',
                'password' => 'nullable|string|min:6',
                'status' => 'required|string|in:Active,Inactive',
                'roles' => 'required|min:1',
                'roles.*' => 'required|exists:roles,id',
            ],[
                'roles.*.exists' => 'Role Assignment Failed'
            ]);
        }

        $user->name = $request->name;
        $user->email = $request->email;
        $user->status = $request->status;

        $user->syncRoles($request->roles);

        if($request->password){
            $user->password = bcrypt($request->password);
            // Mail::to($user->email)->send(new UserUpdated($user,$request->password));
        }
        
        $user->save();

        $request->session()->flash('success', 'User Updated Successfully');

        return redirect()->route('users.index');        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $this->authorize('destroy',User::class);
        
        $user = User::findOrFail($id);
        $user->delete();
        
        $request->session()->flash('success', 'User Deleted Successfully');

        return redirect()->back();
    }
}
