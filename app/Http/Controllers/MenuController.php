<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Menu;
use App\Role;
use Auth;

class MenuController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $this->authorize('view',Menu::class);

        $menus = Menu::orderBy('parent_id')->orderBy('order')->get();
        $roles = Role::get();

        return view('menus.index',compact('menus','roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create',Menu::class);

        $request->validate([
            'name' => 'required|string|max:191',
            'link' => 'required|string|max:191',
            'parent_id' => 'nullable|exists:menus,id',
            'roles.*' => 'required|exists:roles,id',
        ],[
            'roles.*.exists' => 'Role Assignment Failed'
        ]);

        $menu = new Menu;
        $menu->name = $request->name;
        $menu->link = $request->link;
        $menu->isGroup = 0;
        $menu->parent_id = $request->parent_id ? : 0;
        $menu->order = Menu::where('parent_id',$menu->parent_id)->max('order') + 1;
        $menu->save();

        $menu->roles()->attach($request->roles);

        $request->session()->flash('success', 'New Menu Created');

        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('update',Menu::class);

        $menu = Menu::findOrFail($id);
        $roles = Role::get();

        return view('menus.edit',compact('menu','roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('update',Menu::class);

        $request->validate([
            'name' => 'required|string|max:191',
            'link' => 'required|string|max:191',
            'roles.*' => 'required|exists:roles,id',
        ],[
            'roles.*.exists' => 'Role Assignment Failed'
        ]);

        $menu = Menu::findOrFail($id);        
        $menu->name = $request->name;
        $menu->link = $request->link;
        $menu->isGroup = 0;
        $menu->save();

        $menu->roles()->sync($request->roles);

        $request->session()->flash('success', 'Menu Updated Successfully');

        return redirect()->route('menus.index');
    }

    public function updatePriority(Request $request){
        $this->authorize('update',Menu::class);

        $params = array();
        parse_str($_POST['data'], $params);

        $i = 1;

        foreach($params['item'] as $item) {
            $menu = Menu::find($item);
            $menu->order = $i;
            $menu->save();

            $i++;
        }
        return;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $this->authorize('destroy',Menu::class);

        $this->deleteChildren($id);

        $menu = Menu::find($id);
        if($menu) $menu->delete();
        
        $request->session()->flash('success', 'Menu Deleted Successfully');

        return redirect()->back();
    }

    private function deleteChildren($id)
    {
        if(Menu::where('parent_id',$id)->count() == 0)
        {
            Menu::where('id',$id)->delete();
        }
        else
        {
            $menus=Menu::where('parent_id',$id)->get();
            foreach ($menus as $b) {
                $this->deleteChildren($b->id);    
            }
        }
        
    }
}
