<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataTables\TasksDataTable;
use App\DataTables\MyTasksDataTable;
use App\Services\ProjectService;
use App\Services\TaskService;
use App\Status;
use App\Task;
use App\Project;
use App\Team;
use Auth;
use Carbon\Carbon;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(Request $request)
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $this->authorize('view',Task::class);

        $tasks = new TasksDataTable;
        $mytasks = new MyTasksDataTable;
        
        $type = request()->get('type');
        if ($type == 'mytasks') {
            return $mytasks->render('tasks.index');
        }
        
        else if ($type == 'tasks') {
            return $tasks->render('tasks.index');
        }

        $statuses = Status::where('modal','Task')->get();
        $teams = Team::get();
        $projects=ProjectService::projects();

        return view('tasks.index',compact('statuses','teams','projects', 'tasks', 'mytasks'));
    }

    public function kanban(){
        $this->authorize('view',Task::class);

        $statuses = Status::where('modal','Task')->get();
        $tasks = TaskService::tasks();

        return view('tasks.kanban',compact('statuses','tasks'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $project=Project::findOrFail($request->project);
        $this->authorize('createtask',$project);
        
        $request->validate([
            'name' => 'required|string|max:191',
            'description' => 'required',
            'priority' => 'required|integer|min:0',
            'estimated_hrs'=>'required|integer|min:0',
            'estimated_mins'=>'required|integer|min:0|max:59',
            'status' => 'required|exists:statuses,id',
            'project' => 'required|exists:projects,id',
            'team' => 'required|exists:teams,id',
            'deadline' =>'required|date|after:today',


        ],[
            'team.exists' => 'Team Assignment Failed',
            'project.exists' => 'Project Assignment Failed',
            'status.exists' => 'Status Assignment Failed'

        ]);
        $task = new Task;
        $task->title = $request->name;
        $task->description = $request->description;
        $task->priority = $request->priority;
        $task->estimated_minutes = intval($request->estimated_hrs * 60 + $request->estimated_mins) ;        

        $task->deadline = $request->deadline;
        $task->status = $request->status;
        $task->project_id = $request->project;
        $task->team_id = $request->team;
        $task->created_by = Auth::user()->id;

        $task->save();

        $request->session()->flash('success', 'Task Added Successfully');

        return redirect()->back();
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $task = Task::findOrFail($id);

        $this->authorize('update',$task);

        $statuses = Status::where('modal','Task')->get();
        $teams = Team::get();
        $projects=Project::get();

        return view('tasks.edit',compact('task','teams','projects','statuses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $task = Task::findOrFail($id);

        $this->authorize('update',$task);
        
        $request->validate([
            'name' => 'required|string|max:191',
            'description' => 'required',
            'priority' => 'required|integer|min:0',
            'estimated_hrs'=>'required|integer|min:0',
            'estimated_mins'=>'required|integer|min:0|max:59',
            // 'status' => 'required|exists:statuses,id',
            'project' => 'required|exists:projects,id',
            'team' => 'required|exists:teams,id',
            'deadline' =>'required|date|after:today',


        ],[
            'team.exists' => 'Team Assignment Failed',
            'project.exists' => 'Project Assignment Failed',
            // 'status.exists' => 'Status Assignment Failed'

        ]);
        $task->title = $request->name;
        $task->description = $request->description;
        $task->priority = $request->priority;
        $task->estimated_minutes = intval($request->estimated_hrs * 60 + $request->estimated_mins) ;        

        $task->deadline = $request->deadline;
        // $task->status = $request->status;
        $task->project_id = $request->project;
        $task->team_id = $request->team;

        $task->save();

        $request->session()->flash('success', 'Task Updated Successfully');

        return redirect()->route('tasks.index');
    }

    public function updateStatus(Request $request){
        
        $task = Task::findOrFail($request->task_id);        

        $this->authorize('updateStatus',$task);
        
        $request->validate([
            'task_id' => 'required|exists:tasks,id',
            'status_id' => 'required|exists:statuses,id',
            'percent_completed' => 'required|numeric|min:0|max:100',
            'working_hrs'=>'required|integer|min:0',
            'working_mins'=>'required|integer|min:0|max:59',
        ]);

        $task = Task::findOrFail($request->task_id);        
        $task->status = $request->status_id;
        $task->percent_completed = $request->percent_completed;

        if($task->status == 7)
            $task->completed_date = Carbon::now();

        $task->save();

        $task->logs()->create([
            'comments' => $request->comment,
            'status' => $request->status_id,
            'user_id' => Auth::user()->id,
            'percent_completed' => $request->percent_completed,
            'working_hours' => intval($request->working_hrs * 60 + $request->working_mins)

        ]);

        return response()->json('status updated', 200);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $this->authorize('destroy',Task::class);
        
        $task = Task::findOrFail($id);
        $task->delete();
        
        $request->session()->flash('success', 'Task Deleted Successfully');

        return redirect()->back();
    }
}
