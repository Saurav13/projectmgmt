<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataTables\ContactDetailsDataTable;
use App\Services\ProjectService;
use App\Project;
use App\ContactDetail;
use App\Status;
use Auth;


class ContactDetailsController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('view',ContactDetail::class);

        $dataTable = new ContactDetailsDataTable;
        $projects = ProjectService::projects();
       
        return $dataTable->render('contactdetails.index',compact('projects'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        $this->authorize('view',ContactDetail::class);

        $contactdetail=ContactDetail::findOrFail($id);
        return view('contactdetails.show',compact('contactdetail'));
    }
    
    public function store(Request $request)
    {
        $project=Project::findOrFail($request->project);
        $this->authorize('create_contact_details',$project);
        
        $request->validate([
            'name' => 'required|string|max:191',
            'designation' => 'required',
            'contact' => 'required|string|max:191',
            'status' => 'required|in:0,1',
            'project' => 'required|min:1',
            'project.*' => 'required|exists:projects,id',
        ],[
            'project.*.exists' => 'Project Assignment Failed'
        ]);

        $contactdetail = new ContactDetail;
        $contactdetail->name = $request->name;
        $contactdetail->designation = $request->designation;
        $contactdetail->contact = $request->contact;
        $contactdetail->status = $request->status;
        $contactdetail->project_id=$request->project;
        $contactdetail->created_by = Auth::user()->id;
        $contactdetail->updated_by = Auth::user()->id;

        $contactdetail->save();

        $request->session()->flash('success', 'ContactDetail Added Successfully');

        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $contactdetail = ContactDetail::findOrFail($id);

        $this->authorize('update',$contactdetail);
        
        $projects = Project::get();

        return view('contactdetails.edit',compact('contactdetail','projects'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $contactdetail = ContactDetail::findOrFail($id);

        $this->authorize('update',$contactdetail);
        
        $request->validate([
            'name' => 'required|string|max:191',
            'designation' => 'required',
            'contact' => 'required|string|max:191',
            'status' => 'required|in:0,1',
            'project' => 'required|min:1',
            'project.*' => 'required|exists:projects,id',
        ],[
            'project.*.exists' => 'Project Assignment Failed'
        ]);

        $contactdetail->name = $request->name;
        $contactdetail->designation = $request->designation;
        $contactdetail->contact = $request->contact;
        $contactdetail->status = $request->status;
        $contactdetail->project_id=$request->project_id;
        $contactdetail->created_by = Auth::user()->id;
        $contactdetail->updated_by = Auth::user()->id;

        $contactdetail->save();


        $request->session()->flash('success', 'ContactDetail Updated Successfully');

        return redirect()->route('contactdetails.index');        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $contactdetail = ContactDetail::findOrFail($id);

        $this->authorize('destroy',$contactdetail);
        
        $contactdetail->status=0;
        $contactdetail->save();
        
        $request->session()->flash('success', 'ContactDetail Deleted Successfully');

        return redirect()->back();
    }
}
