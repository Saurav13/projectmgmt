<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use App\ProjectLog;
use App\ProjectFile;
use App\Services\ProjectService;
use App\Services\ProjectLogService;
use App\Status;
use Auth;

class ProjectLogController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $this->authorize('view',ProjectLog::class);

        $projects=ProjectService::projects();
        
        $projectlogs = ProjectLogService::projectlogs();
        $statuses = Status::where('modal','Project')->get();
        // dd($projectlogs);
        return view('projectlogs.index',compact('projectlogs','projects','statuses'));

    }

    public function getFile($filename)
    {
        $this->authorize('view',ProjectLog::class);

        try{
            $path = \Storage::get('public/project_files/'.$filename);
            $mimetype = \Storage::mimeType('public/project_files/'.$filename);

            return \Response::make($path, 200, ['Content-Type' => $mimetype]);
        }
        catch (\Exception $e) {
            abort(404);
        }
    }

    public function  listLogsByProject(Request $request)
    {
        $for_project=Project::findOrFail($request->id);

         $this->authorize('viewlog',$for_project);

        $projects=Project::get();
        $projectlogs = ProjectLog::where('project_id',$request->id)->orderBy('created_at','desc')->paginate(10);
        $statuses = Status::where('modal','Project')->get();      

        return view('projectlogs.index',compact('projectlogs','for_project','projects','statuses'));

    }

    public function store(Request $request)
    {
        $project=Project::findOrFail($request->project);
        $this->authorize('createlog',$project);

        $request->validate([
            'description' => 'required',
            'project' => 'required|min:1',
            'status'=>'required|min:1',
            'project.*' => 'required|exists:projects,id',
            'status.*' => 'required|exists:statuses,id',
        ],[
            'project.*.exists' => 'Project Assignment Failed',
            'statuses.*.exists' => 'Status Assignment Failed'

        ]);

        $latestStatus= ProjectLog::where('project_id',$request->project)->latest()->first();

        if($latestStatus)
            $statusChanged = $latestStatus->status != $request->status;
        else
            $statusChanged = false;            

        $com= new ProjectLog;
        $com->description=$request->description;
        $com->project_id=$request->project;
        $com->status=$request->status;
        $com->created_by = Auth::user()->id;
        $com->save();

        if($statusChanged)
        {
            Project::where('id',$request->project)->update(['status'=>$request->status]);
        }

        if($request->hasFile('files')){
            $images = $request->file('files');

            $count = 0;
            foreach($images as $image){
                $file = new ProjectFile;

                $filenameWithExt = $image->getClientOriginalName();
                $name = pathinfo($filenameWithExt, PATHINFO_FILENAME);

                $filename = str_replace(' ','',$name).'_'.time().$count++ . '.' . $image->getClientOriginalExtension();
                $path = $image->storeAs('public/project_files', $filename);
                $file->filepath = $filename;
                $file->projectlog_id = $com->id;
                $file->save();
            }
        }

        $request->session()->flash('success', $statusChanged?'Project Log Added and Project Status Updated Successfully':'Project log Added Successfully');

        return redirect()->back();

        
    }

    public function edit($id)
    {

        $projectlog=ProjectLog::findOrFail($id);
        $this->authorize('update',$projectlog);

        return view('projectlogs.edit',compact('projectlog'));
    }

    public function update(Request $request,$id)
    {

        $request->validate([
            'description' => 'required',
           
        ]

        );

        $com= ProjectLog::findOrFail($id);

        $this->authorize('update',ProjectLog::class);

        $com->description=$request->description;
        $com->updated_by=Auth::user()->id;
        $com->save();

        $request->session()->flash('success', 'Project log Edited Successfully');

        return redirect()->to(route('projectlogs.index'));   
    }
}
