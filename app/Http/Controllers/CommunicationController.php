<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use App\Services\ProjectService;
use App\Services\ProjectCommunicationService;
use App\ProjectCommunication;
use App\ContactDetail;
use Auth;

class CommunicationController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $this->authorize('view',ProjectCommunication::class);
        $projects=ProjectService::projects();
        $communications = ProjectCommunicationService::projectcommunications();
        $contactdetails=ContactDetail::get();
        // dd($communications);
        return view('communications.index',compact('communications','projects','contactdetails'));

    }

    public function  listCommunicationByProject(Request $request)
    {
        $for_project=Project::findOrFail($request->id);

         $this->authorize('viewcommunications',$for_project);
        $projects=Project::get();
        $communications = ProjectCommunication::where('project_id',$request->id)->orderBy('created_at','desc')->paginate(10);
        $contactdetails=ContactDetail::get();

        return view('communications.index',compact('communications','for_project','projects','contactdetails'));

    }

    public function store(Request $request)
    {
        $project=Project::findOrFail($request->project);
        $this->authorize('create_project_communications',$project);

        $request->validate([
            'description' => 'required',
            'project' => 'required|min:1',
            'contactdetail'=>'required|min:1',
            'contactdetail.*' => 'required|exists:contactdetails,id',
        ],[
            'project.*.exists' => 'Project Assignment Failed',
            'contactdetail.*.exists' => 'Contact Details Assignment Failed'

        ]);

        $com= new ProjectCommunication;
        $com->description=$request->description;
        $com->project_id=$request->project;
        $com->contactdetail_id=$request->contactdetail;
        $com->created_by = Auth::user()->id;
        $com->updated_by=Auth::user()->id;
        $com->save();

        $request->session()->flash('success', 'Communication log Added Successfully');

        return redirect()->back();

        
    }

    public function edit($id)
    {

        $communication=ProjectCommunication::findOrFail($id);
        $this->authorize('update',$communication);

        return view('communications.edit',compact('communication'));
    }

    public function update(Request $request,$id)
    {
        $com= ProjectCommunication::findOrFail($id);

        $request->validate([
            'description' => 'required',
           
        ]

        );
        $this->authorize('update',$com);
        
        $com->description=$request->description;
        $com->updated_by=Auth::user()->id;
        $com->save();

        $request->session()->flash('success', 'Communication log Edited Successfully');

        return redirect()->to(route('communications.index'));   
    }
}
