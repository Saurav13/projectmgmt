<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TaskLog;
use App\Services\TaskLogService;
use App\Services\ProjectService;
use App\Services\TaskService;
use App\TaskFile;
use App\Task;
use App\Status;
use Auth;

class TaskLogController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $this->authorize('view',TaskLog::class);

        // $tasks=TaskService::tasks_for_create();
        $tasklogs = TaskLogService::tasklogs();
        $statuses = Status::where('modal','Task')->where('id','!=',7)->get();
        
        $projects = ProjectService::projects();

        return view('tasklogs.index',compact('tasklogs','projects','statuses'));

    }

    public function getFile($filename)
    {
        $this->authorize('view',TaskLog::class);

        try{
            $path = \Storage::get('public/task_files/'.$filename);
            $mimetype = \Storage::mimeType('public/task_files/'.$filename);

            return \Response::make($path, 200, ['Content-Type' => $mimetype]);
        }
        catch (\Exception $e) {
            abort(404);
        }
    }

    public function  listLogsByTask(Request $request)
    {
        $for_task=Task::findOrFail($request->id);

        $this->authorize('viewlog',$for_task);

        $tasks=TaskService::tasks_for_create();

        $tasklogs = TaskLog::where('task_id',$request->id)->orderBy('created_at','desc')->paginate(10);
        $statuses = Status::where('modal','Task')->where('id','!=',7)->get();      

        $projects = ProjectService::projects();
        return view('tasklogs.index',compact('tasklogs','for_task','tasks','statuses','projects'));
    }

    public function store(Request $request)
    {
        $task=Task::findOrFail($request->task);
        $this->authorize('createlog',$task);

        $request->validate([
            'comments' => 'required',
            'task' => 'required|exists:tasks,id',
            'percent_completed' => 'required|numeric|min:0|max:100',
            'working_hrs'=>'required|integer|min:0',
            'working_mins'=>'required|integer|min:0|max:59',
            'status' => 'required|exists:statuses,id|not_in:7',
        ],[
            'task.exists' => 'Task Assignment Failed',
            'status.exists' => 'Status Assignment Failed',
            'status.not_in' => 'Status Assignment Failed'

        ]);

        // $latestStatus= TaskLog::where('task_id',$request->task)->latest()->first()->status;

        // $statusChanged=$latestStatus!=$request->status;

        $com= new TaskLog;
        $com->comments=$request->comments;
        $com->task_id=$request->task;
        $com->status=$request->status;
        $com->percent_completed = $request->percent_completed;
        $com->working_hours = intval($request->working_hrs * 60 + $request->working_mins) ;

        $com->user_id = Auth::user()->id;
        $com->save();

        if($request->hasFile('files')){
            $images = $request->file('files');

            $count = 0;
            foreach($images as $image){
                $file = new TaskFile;

                $filenameWithExt = $image->getClientOriginalName();
                $name = pathinfo($filenameWithExt, PATHINFO_FILENAME);

                $filename = str_replace(' ','',$name).'_'.time().$count++ . '.' . $image->getClientOriginalExtension();
                $path = $image->storeAs('public/task_files', $filename);
                $file->filepath = $filename;
                $file->tasklog_id = $com->id;
                $file->save();
            }
        }

        // if($statusChanged)
        // {
        //     Task::where('id',$request->task)->update(['status'=>$request->status]);
        // }

        // $request->session()->flash('success', $statusChanged?'Task Log Added and Task Status Updated Successfully':'Task log Added Successfully');
        $request->session()->flash('success', 'Task log Added Successfully');

        return redirect()->back();
    }

    public function edit($id)
    {
        $this->authorize('update',TaskLog::class);

        $tasklog=TaskLog::findOrFail($id);
        return view('tasklogs.edit',compact('tasklog'));
    }

    public function update(Request $request,$id)
    {
        $this->authorize('update',TaskLog::class);

        $request->validate([
            'comments' => 'required',
            'task' => 'required|exists:tasks,id',
            'percent_completed' => 'required|numeric|min:0|max:100',
            'working_hours' => 'required|numeric|min:0',
            'status'=>'required|exists:statuses,id'
        ],[
            'task.exists' => 'Task Assignment Failed',
            'status.exists' => 'Status Assignment Failed'

        ]);

        $com= TaskLog::findOrFail($id);
        $com->comments=$request->comments;
        $com->task_id=$request->task;
        $com->status=$request->status;
        $com->percent_completed = $request->percent_completed;
        $com->working_hours = $request->working_hours;

        $com->save();

        $request->session()->flash('success', 'Task log Edited Successfully');

        return redirect()->to(route('tasklogs.index'));   
    }
}
