<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DataTables\RolesDataTable;
use App\Role;

class RoleController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('view',Role::class);
        $roles = Role::get();

        $dataTable = new RolesDataTable;
        $permissionsTree = Role::getPermissionsTree();
        return $dataTable->render('roles.index',compact('roles','permissionsTree'));
    }

    public function create(Request $request)
    {
        $this->authorize('create',Role::class);
        
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create',Role::class);

        $request->validate([
            'name' => 'required|string|max:191',
            'permissions' => 'required|min:1',
            'permissions.*' => 'required|exists:permissions,id',
        ],[
            'permissions.*.exists' => 'Permission Assignment Failed'
        ]);
        
        $role = new Role;
        $role->name = $request->name;

        $role->save();
        $role->permissions()->sync($request->permissions);

        $request->session()->flash('success', 'Role Created Successfully');

        return redirect()->route('roles.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('update',Role::class);

        $role = Role::findOrFail($id);

        $permissionsTree = Role::getPermissionsTree();

        return view('roles.edit',compact('role','permissionsTree'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('update',Role::class);

        $request->validate([
            'name' => 'required|string|max:191',
            'permissions' => 'required|min:1',
            'permissions.*' => 'required|exists:permissions,id',
        ],[
            'permissions.*.exists' => 'Permission Assignment Failed'
        ]);
        
        $role = Role::findOrFail($id);
        $role->name = $request->name;
        $role->permissions()->sync($request->permissions);
        $role->save();
        
        $request->session()->flash('success', 'Role Updated Successfully');

        return redirect()->route('roles.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $this->authorize('destroy',Role::class);
        
        $role = Role::findOrFail($id);
        $role->delete();
        
        $request->session()->flash('success', 'Role Deleted Successfully');

        return redirect()->back();
    }
}
