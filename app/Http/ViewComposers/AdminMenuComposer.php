<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\ContactUsMessage;
use App\Menu;
use Auth;

class AdminMenuComposer
{
    /**
     * Create a movie composer.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $sno = 0;
        $mno = 0;
        // $mno = ContactUsMessage::where('seen','0')->count();
        // if (Auth::user()->roles()->where('title', '=', 'Service Support Messages')->exists())
        //     $sno = ServiceSupportMessage::where('seen','0')->count();
        $menuList = Menu::orderBy('parent_id')->orderBy('order')->get();

        $view->with('mno',$mno)->with('menuList',$menuList);
    }
}