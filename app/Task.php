<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Task extends Model
{
    use SoftDeletes;
    public function state()
    {
        return $this->belongsTo('App\Status', 'status');
    }

    public function creator()
    {
        return $this->belongsTo('App\User','created_by')->withTrashed();
    }

    public function team()
    {
        return $this->belongsTo('App\Team')->withTrashed();
    }

    public function project()
    {
        return $this->belongsTo('App\Project')->withTrashed();
    }
    
    public function logs(){
        return $this->hasMany('App\TaskLog');
    }
}
