<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Team extends Model
{
    use SoftDeletes;
    public function members()
    {
        return $this->belongsToMany('App\User')->withPivot('role_id');
    }

    public function creator()
    {
        return $this->belongsTo('App\User','created_by')->withTrashed();
    }

    public function updator()
    {
        return $this->belongsTo('App\User', 'updated_by')->withTrashed();
    }

    public function projects()
    {
        return $this->belongsToMany('App\Project');
    }
}
