<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use App\Notifications\ResetPasswordNotification;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use SoftDeletes;
    use Notifiable;
    use HasRoles;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function teams()
    {
        return $this->belongsToMany('App\Team')->withPivot('role_id');
    }

    public function teamrole($team_id)
    {
        $role_id= TeamUser::where('user_id',$this->id)->where('team_id',$team_id);
        return Role::findOrFail($role_id);
    }
    public function teamroles($project_id)
    {
        $team_ids=Project::findOrFail($project_id)->teams->pluck('id');
        $role_ids=TeamUser::where('user_id',$this->id)->whereIn('team_id',$team_ids)->get()->pluck('role_id');
        return Role::whereIn('id',$role_ids)->get();
    }

    public function mytasks()
    {
        return $this->belongsToMany('App\Task', 'task_assigns', 'assigned_to', 'task_id');
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }
}
