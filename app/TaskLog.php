<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class TaskLog extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'comments',
        'status',
        'user_id',
        'percent_completed',
        'working_hours'
    ];
    
    public function state()
    {
        return $this->belongsTo('App\Status', 'status');
    }

    public function creator()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function task()
    {
        return $this->belongsTo('App\Task')->withTrashed();
    }

    public function files()
    {
        return $this->hasMany('App\TaskFile', 'tasklog_id');
    }

    public function verification()
    {
        return $this->hasOne('App\TaskVerification','tasklog_id');
    }
}
