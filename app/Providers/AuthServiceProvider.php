<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\User;
use App\Policies\UserPolicy;
use App\Role;
use App\Policies\RolePolicy;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        User::class => UserPolicy::class,
        Role::class => RolePolicy::class,
        \App\Menu::class => \App\Policies\MenuPolicy::class,
        \App\Team::class => \App\Policies\TeamPolicy::class,
        \App\Project::class => \App\Policies\ProjectPolicy::class,
        \App\Task::class => \App\Policies\TaskPolicy::class,
        \App\TaskLog::class => \App\Policies\TaskLogPolicy::class,
        \App\ProjectLog::class => \App\Policies\ProjectLogPolicy::class,
        \App\ContactDetail::class => \App\Policies\ContactDetailPolicy::class,
        \App\ProjectCommunication::class => \App\Policies\ProjectCommunicationPolicy::class,
        \App\TaskAssign::class => \App\Policies\TaskAssignPolicy::class,
        \App\TaskVerification::class => \App\Policies\TaskVerificationPolicy::class,


    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::before(function ($user, $ability) {
            return $user->hasRole('Super Admin') ? true : null;
        });
    }
}
