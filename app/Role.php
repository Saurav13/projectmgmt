<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Role as SpatieRole;
use Spatie\Permission\Models\Permission;

class Role extends SpatieRole
{
    public static function getPermissionsTree()
    {
        $tree = [];

        $permissions = Permission::get();

        foreach ($permissions as $permission) {

            list($model, $action) = explode('.', $permission->name);

            if (!isset($tree[$model])) {
                $tree[$model] = [];
            }
            $tree[$model][$permission->id] = $action;
        }

        return $tree;
    }

    public function list_permissions()
    {
        return $this->belongsToMany('App\Permission','role_has_permissions');
    }
}
