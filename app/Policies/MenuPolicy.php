<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class MenuPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the menu.
     *
     * @param  \App\User  $user
     * @param  \App\Menu  $menu
     * @return mixed
     */
    public function view(User $user)
    {
        if ($user->can('menu.view')) {
            return true;
        }
        return false;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        return $user->can('menu.create');
    }

    /**
     * @param User $user
     * @param UserModel $usermodel
     * @return bool
     */
    public function update(User $user)
    {
        if ($user->can('menu.update')) {
            return true;
        }
        return false;
    }

    /**
     * @param User $user
     * @param UserModel $usermodel
     * @return bool
     */
    public function destroy(User $user)
    {
        if ($user->can('menu.delete')) {
            return true;
        }
        return false;
    }
}
