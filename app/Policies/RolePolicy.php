<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class RolePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param User $user
     * @return bool
     */
    public function view(User $user)
    {
        if ($user->can('roles.view')) {
            return true;
        }
        return false;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        return $user->can('roles.create');
    }

    /**
     * @param User $user
     * @param Role $role
     * @return bool
     */
    public function update(User $user)
    {
        if ($user->can('roles.update')) {
            return true;
        }
        return false;
    }

    /**
     * @param User $user
     * @param Role $role
     * @return bool
     */
    public function destroy(User $user)
    {
        if ($user->can('roles.delete')) {
            return true;
        }
        return false;
    }
}
