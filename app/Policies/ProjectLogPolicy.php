<?php

namespace App\Policies;

use App\User;
use App\Project;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProjectLogPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param User $user
     * @return bool
     */
    public function view(User $user)
    {
        if ($user->can('project_logs.view')) {
            
                return true;
         
        }
        return false;
    }

    /**
     * @param User $user
     * @return bool
     */

    public function create(User $user)
    {
        // return true;
        if ($user->can('project_logs.create')) {
                return true;
        }
        return false;
    }

   

    /**
     * @param User $user
     * @param UserModel $usermodel
     * @return bool
     */
    public function update(User $user, $project_log)
    {
        if ($user->can('project_logs.update')) {
            if($project->created_by==$user->id)
                return true;
        }
        return false;
    }

    
    
}
