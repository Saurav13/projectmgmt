<?php

namespace App\Policies;

use App\User;
use App\ContactDetail;
use Illuminate\Auth\Access\HandlesAuthorization;

class ContactDetailPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the contactDetail.
     *
     * @param  \App\User  $user
     * @param  \App\ContactDetail  $contactDetail
     * @return mixed
     */
    public function view(User $user)
    {
        // return false;
        return $user->can('contact_details.view');
    }

    /**
     * Determine whether the user can create contactDetails.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can('contact_details.create');
    }

    /**
     * Determine whether the user can update the contactDetail.
     *
     * @param  \App\User  $user
     * @param  \App\ContactDetail  $contactDetail
     * @return mixed
     */
    public function update(User $user,$contactdetail)
    {
        if($user->can('contact_details.update')){
       
            if($contactdetail->project->teams->intersect($user->teams)->count()>0)
                return true;
        }
        return false;
    }

    /**
     * Determine whether the user can delete the contactDetail.
     *
     * @param  \App\User  $user
     * @param  \App\ContactDetail  $contactDetail
     * @return mixed
     */
    public function destroy(User $user,$contact_detail)
    {
        if($user->can('contact_details.delete')){
            if($contact_detail->project->teams->intersect($user->teams)->count()>0)
                return true;
        }
        return false;
    }
}
