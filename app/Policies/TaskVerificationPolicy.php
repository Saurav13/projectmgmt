<?php

namespace App\Policies;

use App\User;
use App\TaskVerification;
use Illuminate\Auth\Access\HandlesAuthorization;

class TaskVerificationPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the taskVerification.
     *
     * @param  \App\User  $user
     * @param  \App\TaskVerification  $taskVerification
     * @return mixed
     */
    public function view(User $user)
    {
        // return true;
        if ($user->can('task_verify.view')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can create taskVerifications.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the taskVerification.
     *
     * @param  \App\User  $user
     * @param  \App\TaskVerification  $taskVerification
     * @return mixed
     */
    public function update(User $user, TaskVerification $taskVerification)
    {
        //
    }

    /**
     * Determine whether the user can delete the taskVerification.
     *
     * @param  \App\User  $user
     * @param  \App\TaskVerification  $taskVerification
     * @return mixed
     */
    public function delete(User $user, TaskVerification $taskVerification)
    {
        //
    }
}
