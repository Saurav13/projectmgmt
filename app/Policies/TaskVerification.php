<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TaskVerification
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param User $user
     * @return bool
     */
    public function view(User $user)
    {
        if ($user->can('tasks_verifications.view')) {
            return true;
        }
        return false;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        return $user->can('tasks_verifications.create');
    }

    /**
     * @param User $user
     * @param UserModel $usermodel
     * @return bool
     */
    public function update(User $user)
    {
        if ($user->can('tasks_verifications.update')) {
            return true;
        }
        return false;
    }

    /**
     * @param User $user
     * @param UserModel $usermodel
     * @return bool
     */
    public function destroy(User $user)
    {
        if ($user->can('tasks_verifications.delete')) {
            return true;
        }
        return false;
    }
}
