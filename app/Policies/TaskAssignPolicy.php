<?php

namespace App\Policies;

use App\User;
use App\TaskAssign;
use Illuminate\Auth\Access\HandlesAuthorization;

class TaskAssignPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the taskAssign.
     *
     * @param  \App\User  $user
     * @param  \App\TaskAssign  $taskAssign
     * @return mixed
     */
    public function view(User $user)
    {
        if ($user->can('task_assigns.view')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can create taskAssigns.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can('task_assigns.create');
        
    }

    /**
     * Determine whether the user can update the taskAssign.
     *
     * @param  \App\User  $user
     * @param  \App\TaskAssign  $taskAssign
     * @return mixed
     */
    public function update(User $user, TaskAssign $taskAssign)
    {
        //
    }

    /**
     * Determine whether the user can delete the taskAssign.
     *
     * @param  \App\User  $user
     * @param  \App\TaskAssign  $taskAssign
     * @return mixed
     */
    public function delete(User $user, TaskAssign $taskAssign)
    {
        //
    }
}
