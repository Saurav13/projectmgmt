<?php

namespace App\Policies;

use App\User;
use App\Project;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProjectPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param User $user
     * @return bool
     */
    public function view(User $user)
    {
        if ($user->can('projects.view')) {
            $teams_from_user= $user->teams;

            return true;
        }
        return false;
    }

    public function viewlog(User $user, Project $project)
    {
        if ($user->can('project_logs.view')) {
            
            if($project->teams->intersect($user->teams)->count()>0)
                return true;
            else if($project->created_by==$user->id)
                return true;
        }
        return false;
    }
    public function viewcommunications(User $user, Project $project)
    {
        if ($user->can('project_communications.view')) {
            
            if($project->teams->intersect($user->teams)->count()>0)
                return true;
            else if($project->created_by==$user->id)
                return true;
        }
        return false;
    }

    public function createlog(User $user,$project)
    {
        if ($user->can('project_logs.create')) {
            
            if($project->teams->intersect($user->teams)->count()>0)
                return true;
            else if($project->created_by==$user->id)
                return true;
        }
        return false;
    }

    public function createtask(User $user,$project)
    {
        if ($user->can('tasks.create')) {
            
            if($project->teams->intersect($user->teams)->count()>0)
                return true;
            else if($project->created_by==$user->id)
                return true;
        }
        return false;
    }

    public function create_contact_details(User $user,$project)
    {
        if ($user->can('project_logs.create')) {
            
            if($project->teams->intersect($user->teams)->count()>0)
                return true;
            else if($project->created_by==$user->id)
                return true;
        }
        return false;
    }

    public function create_project_communications(User $user,$project)
    {
        if ($user->can('project_communications.create')) {
            
            if($project->teams->intersect($user->teams)->count()>0)
                return true;
            else if($project->created_by==$user->id)
                return true;
        }
        return false;
    }




    public function singleview(User $user,$project)
    {
        if($project->createdBy==$user->id)
            return true;
        else if($project->teams->intersect($user->teams)->count()>0)
            return true;
        return false;
    }


    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        // return true;
        // dd($user->can('projects.create'));
        return $user->can('projects.create');
    }

    /**
     * @param User $user
     * @param UserModel $usermodel
     * @return bool
     */
    public function update(User $user)
    {
        if ($user->can('projects.update')) {
            return true;
        }
        return false;
    }

    /**
     * @param User $user
     * @param UserModel $usermodel
     * @return bool
     */
    public function destroy(User $user)
    {
        if ($user->can('projects.delete')) {
            return true;
        }
        return false;
    }

    public function updateStatus(User $user, $project)
    {
         
        if($user->can('projects.updateStatus')){
            if($project->createdBy==$user->id)
                return true;
            else if($project->teams->intersect($user->teams)->count()>0 && $this->hasPermission($user->teamroles($project->id),'projects.updateStatus'))
                return true;
        }
        return false;
    }

    private function hasPermission($roles, $permission)
    {
        foreach($roles as $role){
            if(in_array($permission,$role->list_permissions->pluck('name')->toArray()))
                return true;
        }
        return false;
    }
    
}
