<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TaskPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param User $user
     * @return bool
     */
    public function view(User $user)
    {
        if ($user->can('tasks.view')) {
            return true;
        }
        return false;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        return $user->can('tasks.create');
    }

    public function createlog(User $user,$task)
    {
        if ($user->can('task_logs.create')) {
            if(in_array($task->team,$user->teams->toArray()))
                return true;
        else if($task->created_by==$user->id)
            return true;

        }
        return false;
    }

    public function viewlog(User $user,$task)
    {
        // return true;
        if ($user->can('task_logs.view')) {
        if($task->project->teams->intersect($user->teams)->count()>0)
            return true;
        else if($task->project->created_by==$user->id)
            return true;
        else if($task->created_by==$user->id)
            return true;
        }
       return false;
    }

    /**
     * @param User $user
     * @param UserModel $usermodel
     * @return bool
     */
    public function update(User $user,$task)
    {
        if ($user->can('tasks.update')) {
            if($task->created_by==$user->id &&$task->status!=7)
                return true;
        }
        return false;
    }

    public function updateStatus(User $user,$task)
    {
        if ($user->can('tasks.updateStatus')) {
       
            if(in_array($task->team,$user->teams->toArray()))
                return true;
            else if($task->created_by==$user->id)
                return true;
            else if($task->project->created_by==$user->id)
                return true;
        }
        return false;
    }

    public function taskassign(User $user,$task)
    {
        if($user->can('task_assigns.create'))
        {
            if(in_array($task->team,$user->teams->toArray()))
                return true;
            else if($task->created_by==$user->id)
                return true;
            else if($task->project->created_by==$user->id)
                return true;
        }
        return false;
    }

    /**
     * @param User $user
     * @param UserModel $usermodel
     * @return bool
     */
    public function destroy(User $user, $task)
    {
        if ($user->can('tasks.delete')) {
            if($task->created_by==$user->id && $task->status!=7)
                return true;
        }
        return false;
    }
}
