<?php

namespace App\Policies;

use App\User;
use App\ProjectCommunication;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProjectCommunicationPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the communication.
     *
     * @param  \App\User  $user
     * @param  \App\Communication  $communication
     * @return mixed
     */
    public function view(User $user)
    {
        if ($user->can('project_communications.view')) {
                
                return true;
        
        }
        return false;
    }

    /**
     * Determine whether the user can create communications.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        if ($user->can('project_communications.create')) {
            return true;
    }
    return false;
        
    }

    /**
     * Determine whether the user can update the communication.
     *
     * @param  \App\User  $user
     * @param  \App\Communication  $communication
     * @return mixed
     */
    public function update(User $user, ProjectCommunication $communication)
    {
        // return true;
        if ($user->can('project_communications.update')) {
            // return true;
            if($communication->created_by==$user->id)
                return true;
        }
        return false;
    }

    /**
     * Determine whether the user can delete the communication.
     *
     * @param  \App\User  $user
     * @param  \App\Communication  $communication
     * @return mixed
     */
    public function delete(User $user, ProjectCommunication $communication)
    {
        //
    }
}
