<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TaskLogPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param User $user
     * @return bool
     */
    public function view(User $user)
    {
        // return true;
        if ($user->can('task_logs.view')) {
            return true;
        }
        return false;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        return $user->can('task_logs.create');
    }

    /**
     * @param User $user
     * @param UserModel $usermodel
     * @return bool
     */
    public function update(User $user)
    {
        if ($user->can('task_logs.update')) {
            return true;
        }
        return false;
    }

    public function taskverify(User $user,$tasklog)
    {
        if ($user->can('task_verify.create')) {
            if(in_array($tasklog->task->team,$user->teams->toArray()))
                return true;
        }
        return false;
    }

    /**
     * @param User $user
     * @param UserModel $usermodel
     * @return bool
     */
    public function destroy(User $user)
    {
        if ($user->can('tasklogs.delete')) {
            return true;
        }
        return false;
    }
}
