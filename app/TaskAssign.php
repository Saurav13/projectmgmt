<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaskAssign extends Model
{
    public function task()
    {
        return $this->belongsTo('App\Task')->withTrashed();
    }

    public function creator()
    {
        return $this->belongsTo('App\User','assigned_by')->withTrashed();
    }

    public function worker()
    {
        return $this->belongsTo('App\User', 'assigned_to')->withTrashed();
    }
}
