<?php

namespace App\Services;

use App\Task;
use App\Project;
use Auth;
use Illuminate\Support\Collection;


class TaskService
{
    public static function tasks()
    {

        $user=Auth::user();
        $tasks= new Collection;

        $teams=Auth::user()->teams->pluck('id');

        $project_with_teams=Project::whereHas('teams',function($query) use($teams){
            $query->whereIn('teams.id',$teams);
        })->pluck('id');
        
        if( !$user->hasRole('Super Admin')){

           $tasks= Task::whereIn('project_id',$project_with_teams)->orderBy('created_at','DESC')->with('project')->get();
           
        }
        else{
            $tasks=Task::with('project')->orderBy('created_at','DESC')->get();
        }
        // $tasks=Task::orderBy('created_at','DESC')->paginate(10);
        
        return $tasks;
    }

    public static function tasks_for_create()
    {

        $user=Auth::user();
        $tasks= new Collection;

        $teams=Auth::user()->teams->pluck('id');
        if( !$user->hasRole('Super Admin')){
            $tasks=Task::where('status','!=',7)->whereIn('team_id',$teams)->get();
            // $tasklogs= TaskLog::whereIn('task_id',$tasks)->orderBy('created_at','DESC')->paginate(10);
           
        }
        else{
            $tasks=Task::where('status','!=',7)->get();
        }
        // $tasks=Task::orderBy('created_at','DESC')->get();
        
        return $tasks;
    }
}