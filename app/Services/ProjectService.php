<?php

namespace App\Services;

use App\Project;
use Auth;
use Illuminate\Support\Collection;


class ProjectService
{
    public static function projects()
    {
        $allprojects = Project::get();

        $user=Auth::user();
        $projects= new Collection;

        if( !$user->hasRole('Super Admin')){

        
            foreach($allprojects as $project)
            {
                if($project->created_by==$user->id)
                    $projects->push($project);
                else if($project->teams->intersect($user->teams)->count()>0)
                    $projects->push($project);
            }
        }
        else{
            $projects=$allprojects;
        }
        // $projects=$allprojects;

        return $projects;
    }

    public static function myProjectCount()
    {
        $allprojects = Project::get();

        $user=Auth::user();
        $projects= new Collection;

        foreach($allprojects as $project)
        {
            if($project->created_by==$user->id)
                $projects->push($project);
            else if($project->teams->intersect($user->teams)->count()>0)
                $projects->push($project);
        }

        return $projects->count();
    }

}