<?php

namespace App\Services;

use App\ProjectLog;
use App\Project;
use Auth;
use Illuminate\Support\Collection;


class ProjectLogService
{
    public static function projectlogs()
    {

        $user=Auth::user();
        $projectlogs= new Collection;

        $teams=Auth::user()->teams->pluck('id');
        $project_with_teams=Project::whereHas('teams',function($query) use($teams){
            $query->whereIn('teams.id',$teams);
        })->pluck('id');
        if( !$user->hasRole('Super Admin')){

           $projectlogs= ProjectLog::whereIn('project_id',$project_with_teams)->orderBy('created_at','DESC')->paginate(10);
           
        }
        else{
            $projectlogs=ProjectLog::orderBy('created_at','DESC')->paginate(10);
        }
        
        return $projectlogs;
    }

}