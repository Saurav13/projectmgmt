<?php

namespace App\Services;

use App\Task;
use App\Project;
use App\TaskLog;
use Auth;
use Illuminate\Support\Collection;


class TaskLogService
{
    public static function tasklogs()
    {

        $user=Auth::user();
        $tasklogs= new Collection;

        $teams=Auth::user()->teams->pluck('id');
        $project_with_teams=Project::whereHas('teams',function($query) use($teams){
            $query->whereIn('teams.id',$teams);
        })->pluck('id');
        $tasks=Task::whereIn('project_id',$project_with_teams)->get()->pluck('id');
        if( !$user->hasRole('Super Admin')){

           $tasklogs= TaskLog::whereIn('task_id',$tasks)->orderBy('created_at','desc')->paginate(10);
           
        }
        else{
            $tasklogs=TaskLog::orderBy('created_at','desc')->paginate(10);
        }
        // $tasks=Task::orderBy('created_at','DESC')->paginate(10);
        
        return $tasklogs;
    }
   

}