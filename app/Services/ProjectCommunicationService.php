<?php

namespace App\Services;

use App\ProjectCommunication;
use App\Project;
use Auth;
use Illuminate\Support\Collection;


class ProjectCommunicationService
{
    public static function projectcommunications()
    {

        $user=Auth::user();
        $projectcommunications= new Collection;

        $teams=Auth::user()->teams->pluck('id');
        $project_with_teams=Project::whereHas('teams',function($query) use($teams){
            $query->whereIn('teams.id',$teams);
        })->pluck('id');
        if( !$user->hasRole('Super Admin')){

           $projectcommunications= ProjectCommunication::whereIn('project_id',$project_with_teams)->orderBy('created_at','DESC')->paginate(10);
           
        }
        else{
            $projectcommunications=ProjectCommunication::orderBy('created_at','DESC')->paginate(10);
        }
        // $projectcommunications=ProjectCommunication::orderBy('created_at','DESC')->paginate(10);
        
        return $projectcommunications;
    }

}