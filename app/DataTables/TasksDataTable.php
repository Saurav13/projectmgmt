<?php

namespace App\DataTables;

use Yajra\DataTables\Services\DataTable;
use App\Task;
use App\Services\TaskService;

class TasksDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
        ->editColumn('status', function (Task $task) {
            $html = "";

            $html = ($task->status == 7 ? "<span class='tag tag-success'>" : "<span class='tag tag-primary'>").$task->state->status."</span>";
       
            return $html;
        })
        ->editColumn('estimated_minutes', function (Task $task) {
            $estimated_hrs = intval($task->estimated_minutes/60);
            $estimated_mins = intval($task->estimated_minutes%60);

            $text = $estimated_hrs ? $estimated_hrs.' hrs ':'';
            $text .= $estimated_mins ? $estimated_mins.' mins' : '';
            return $text;
        })
        ->editColumn('created_by', function (Task $task) {
            return $task->creator->name;
        })
        ->addColumn('action', function (Task $task) {
            return view('tasks.action', compact('task'));
        })
        ->rawColumns([ 'status','action' ]);

    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Task $model)
    {
        return TaskService::tasks();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->ajax([
                        'url' => '?type=tasks'
                    ])
                    ->columns($this->getColumns())
                    // ->minifiedAjax()
                    ->addAction(['width' => '80px'])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id',
            'title','priority','estimated_minutes','deadline','percent_completed','started_date','completed_date',
            'status', 'created_by'
        ];
    }

    protected function getBuilderParameters(){
        return [
            'dom' => 'Bfrtip',
            'buttons' => ['pageLength', 'excel', 'print','reload'],
            'lengthMenu' => [
                [ 10, 25, 50, 100 ],
                [ '10 rows', '25 rows', '50 rows', '100 rows' ]
            ]
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Tasks_' . date('YmdHis');
    }
}
