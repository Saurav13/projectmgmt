<?php

namespace App\DataTables;

use App\TaskAssignAssign;
use Yajra\DataTables\Services\DataTable;
use App\TaskAssign;

class TaskAssignsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
        ->editColumn('status', function (TaskAssign $task_assign) {
            $html = "";

            $html = ($task_assign->status ? "<span class='tag tag-success'>" : "<span class='tag tag-danger'>").($task_assign->status ? 'Enabled' : 'Handover')."</span>";
       
            return $html;
        })
        ->editColumn('assigned_by', function (TaskAssign $task_assign) {
            return $task_assign->creator->name;
        })
        ->editColumn('assigned_to', function (TaskAssign $task_assign) {
            return $task_assign->worker->name;
        })
        ->addColumn('task', function (TaskAssign $task_assign) {
            return $task_assign->task->title;
        })
        ->addColumn('action', function (TaskAssign $task_assign) {
            return view('task_assigns.action', compact('task_assign'));
        })
        ->rawColumns([ 'status','action' ]);

    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(TaskAssign $model)
    {
        return $model->newQuery()->select('task_assigns.*')->orderBy('created_at','desc');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->addAction(['width' => '80px'])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id','task',
            'assigned_to','assigned_by','status'
        ];
    }

    protected function getBuilderParameters(){
        return [
            'dom' => 'Bfrtip',
            'buttons' => ['pageLength', 'excel', 'print','reload'],
            'lengthMenu' => [
                [ 10, 25, 50, 100 ],
                [ '10 rows', '25 rows', '50 rows', '100 rows' ]
            ]
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'TaskAssignAssigns_' . date('YmdHis');
    }
}
