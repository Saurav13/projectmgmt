<?php

namespace App\DataTables;

use App\User;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class UsersDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);
        return $dataTable
            ->editColumn('status', function (User $user) {
                $html = "";
                
                $html = $user->status == 'Active' ? "<span class='tag tag-success'>Active</span>" : "<span class='tag tag-danger'>InActive</span>";
                return $html;
            })
            ->addColumn('roles', function (User $user) {
                $html = "";
                foreach ($user->roles as $roles) {
                    $html .= "<span class='tag tag-primary' style='margin-right:2px'>". $roles->name ."</span>";
                }
                return $html;
            })
            ->addColumn('action', function (User $user) {
                return view('users.users-action', compact('user'));
            })
            ->rawColumns([ 'status','roles','action' ]);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return mixed
     */
    public function query(User $model)
    {
        $query = $model->with('roles')->select('users.*')->orderBy('created_at','desc');

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax('')
                    ->addAction(['width' => '80px'])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id',
            'name',
            'email',
            'status',
            'roles',
            'created_at',
            'updated_at'
        ];
    }

    protected function getBuilderParameters(){
        return [
            'dom' => 'Bfrtip',
            'buttons' => ['pageLength', 'excel', 'print','reload'],
            'lengthMenu' => [
                [ 10, 25, 50, 100 ],
                [ '10 rows', '25 rows', '50 rows', '100 rows' ]
            ]
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'users_' . time();
    }
}