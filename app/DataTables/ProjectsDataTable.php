<?php

namespace App\DataTables;

use App\Project;
use App\Services\ProjectService;
use Yajra\DataTables\Services\DataTable;

class ProjectsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
        ->editColumn('status', function (Project $project) {
            $html = "";
            
            $html = ($project->status == 1 ? "<span class='tag tag-primary'>" : ($project->status == 2 ? "<span class='tag tag-success'>" : "<span class='tag tag-danger'>")).$project->state->status."</span>";
 
            return $html;
        })
        ->editColumn('created_by', function (Project $project) {
            return $project->creator->name;
        })
        ->editColumn('updated_by', function (Project $project) {
            return $project->updator->name;
        })
        ->addColumn('teams_count', function (Project $project) {
            return $project->teams_count;
        })
        ->addColumn('action', function (Project $project) {
            return view('projects.action', compact('project'));
        })
        ->rawColumns([ 'status','action' ]);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Project $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Project $model)
    {
        return ProjectService::projects();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->addAction(['width' => '80px'])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id',
            'name','company','status','teams_count', 'created_by','created_at', 'updated_by',
            'updated_at'
        ];
    }

    protected function getBuilderParameters(){
        return [
            'dom' => 'Bfrtip',
            'buttons' => ['pageLength', 'excel', 'print','reload'],
            'lengthMenu' => [
                [ 10, 25, 50, 100 ],
                [ '10 rows', '25 rows', '50 rows', '100 rows' ]
            ]
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Projects_' . date('YmdHis');
    }
}
