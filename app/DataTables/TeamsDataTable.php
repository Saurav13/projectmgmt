<?php

namespace App\DataTables;

use App\Team;
use Yajra\DataTables\Services\DataTable;

class TeamsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
        ->editColumn('active', function (Team $team) {
            $html = "";
            
            $html = $team->active ? "<span class='tag tag-success'>Active</span>" : "<span class='tag tag-danger'>InActive</span>";
            return $html;
        })
        ->editColumn('created_by', function (Team $team) {
            return $team->creator->name;
        })
        ->editColumn('updated_by', function (Team $team) {
            return $team->updator->name;
        })
        ->addColumn('members_count', function (Team $team) {
            return $team->members_count;
        })
        ->addColumn('action', function (Team $team) {
            return view('teams.teams-action', compact('team'));
        })
        ->rawColumns([ 'active','action' ]);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Team $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Team $model)
    {
        return $model->newQuery()->select('teams.*')->orderBy('created_at','desc')->withCount('members');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->addAction(['width' => '80px'])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id',
            'name',
            'members_count',
            'active',
            'created_by',
            'created_at',
            'updated_by',
            'updated_at'
        ];
    }

    protected function getBuilderParameters(){
        return [
            'dom' => 'Bfrtip',
            'buttons' => ['pageLength', 'excel', 'print','reload'],
            'lengthMenu' => [
                [ 10, 25, 50, 100 ],
                [ '10 rows', '25 rows', '50 rows', '100 rows' ]
            ]
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Teams_' . date('YmdHis');
    }
}
