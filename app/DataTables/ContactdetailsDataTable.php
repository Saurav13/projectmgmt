<?php

namespace App\DataTables;

use App\ContactDetail;
use Yajra\DataTables\Services\DataTable;

class ContactDetailsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->editColumn('created_by', function (ContactDetail $contactdetail) {
                return $contactdetail->creator->name;
            })
            ->editColumn('updated_by', function (ContactDetail $contactdetail) {
                return $contactdetail->updator->name;
            })
            ->addColumn('teams_count', function (ContactDetail $contactdetail) {
                return $contactdetail->teams_count;
            })
            ->addColumn('action', function (ContactDetail $contactdetail) {
                return view('contactdetails.action', compact('contactdetail'));
            })
            ->rawColumns([ 'action' ]);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\ContactDetail $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(ContactDetail $model)
    {
        return $model->newQuery()->where('status',1)->with('project')->select('id','name', 'designation', 'contact', 'created_at','created_by', 'project_id','updated_by','updated_at');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->addAction(['width' => '80px'])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id',
            'name',
            'designation',
            'contact',
             'created_by','created_at', 'updated_by',
            'updated_at'
        ];
    }
    protected function getBuilderParameters(){
        return [
            'dom' => 'Bfrtip',
            'buttons' => ['pageLength', 'excel', 'print','reload'],
            'lengthMenu' => [
                [ 10, 25, 50, 100 ],
                [ '10 rows', '25 rows', '50 rows', '100 rows' ]
            ]
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'ContactDetails_' . date('YmdHis');
    }
}
