<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{
    use SoftDeletes;
    public function state()
    {
        return $this->belongsTo('App\Status', 'status');
    }

    public function creator()
    {
        return $this->belongsTo('App\User','created_by')->withTrashed();
    }

    public function updator()
    {
        return $this->belongsTo('App\User', 'updated_by')->withTrashed();
    }

    public function teams()
    {
        return $this->belongsToMany('App\Team');
    }

    public function logs(){
        return $this->hasMany('App\ProjectLog');
    }

    public function tasks()
    {
        return $this->hasMany('App\Task');
    }
}
