<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Menu extends Model
{
    public function roles()
    {
        return $this->belongsToMany('App\Role');
    }

    public function getUserCanAccessAttribute()
    {
        $userRoles = Auth::user()->roles->pluck('id');

        $teamRoles = Auth::user()->teams->pluck('pivot.role_id');
        $userRoles = $userRoles->merge($teamRoles);

        $roles = collect($this->roles->pluck('id'));

        if ($roles->isEmpty()) {
            return true;
        }

        $intersection = $roles->intersect($userRoles);

        return $intersection->count() > 0 ? true : false;
    }
}
