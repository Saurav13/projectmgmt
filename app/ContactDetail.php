<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactDetail extends Model
{
    public function creator()
    {
        return $this->belongsTo('App\User','created_by');
    }

    public function updator()
    {
        return $this->belongsTo('App\User', 'updated_by');
    }

    public function project()
    {
        return $this->belongsTo('App\Project','project_id');
    }
}
