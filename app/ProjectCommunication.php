<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProjectCommunication extends Model
{
    use SoftDeletes;
    public function creator()
    {
        return $this->belongsTo('App\User','created_by')->withTrashed();
    }

    public function updator()
    {
        return $this->belongsTo('App\User','updated_by')->withTrashed();
    }

    public function project()
    {
        return $this->belongsTo('App\Project','project_id')->withTrashed();
    }

    public function contactDetail()
    {
        return $this->belongsTo('App\ContactDetail','contactdetail_id');
    }
}
