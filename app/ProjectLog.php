<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectLog extends Model
{
    
    protected $fillable = [
        'description',
        'status',
        'created_by'
    ];

    public function state()
    {
        return $this->belongsTo('App\Status', 'status');
    }

    public function creator()
    {
        return $this->belongsTo('App\User','created_by')->withTrashed();
    }

    public function project()
    {
        return $this->belongsTo('App\Project','project_id')->withTrashed();
    }

    public function files()
    {
        return $this->hasMany('App\ProjectFile','projectlog_id');
    }

}
